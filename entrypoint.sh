#!/bin/sh
set -e
if grep -q '$CMS_BASE_URL' /ng-app/dist/browser/assets/config.local.json; then

envsub --syntax dollar-basic /ng-app/dist/browser/assets/config.local.json /ng-app/dist/browser/assets/config.local.json.temp
mv -f /ng-app/dist/browser/assets/config.local.json.temp /ng-app/dist/browser/assets/config.local.json
#nginx -s reload

fi
exec "$@"