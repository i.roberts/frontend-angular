import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
// import { KeycloakService } from 'keycloak-angular';
import { ConfigService } from '@ngx-config/core';
import { BaseComponent } from '../framework/core';
import { routeAnimation } from '../shared';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  animations: [routeAnimation],
})
export class ContactComponent extends BaseComponent {
  userDetails: any;
  isAuthenticated = false;
  iframeSrc:string = undefined;

  constructor(
    private readonly cd: ChangeDetectorRef,
    // private readonly keycloakService: KeycloakService,
    private readonly config: ConfigService
  ) {
    super();
   }

   ngOnInit():void { 


    this.iframeSrc = `${this.config.getSettings('system.cms_root_url')}/clean-contact-form`;
   }

}
