import { WebformElements, WebFormT, WebformError } from '../../library/webforms/webforms.model';

export interface WebformState {
  webformFields: WebFormT;
  webformElements: WebformElements;
  loading: boolean;
  error: WebformError;
  success: string;
}
