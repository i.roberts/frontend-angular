/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { WebForm, WebformElements, WebformError } from '../../library/webforms/webforms.model';

import * as WebformActions from './webform.actions';
import { WebformState } from './webform.state';

const defaultWebformState: WebformState = {
  loading: false,
  error: {
    error: false,
    errorMessage: ''
  },
  success: '',
  webformFields: WebForm.generateMockWebform(),
  webformElements: WebForm.generateMockWebElements()
};

// tslint:disable-next-line:only-arrow-functions
export function WebformReducer(state: WebformState = defaultWebformState, action: any): WebformState {
  switch (action.type) {
    case WebformActions.WebformActionTypes.GET_WEBFORM_FIELDS: {
      // console.log("GET_WEBFORM_FIELDS");

      return { ...state, loading: true };
    }
    case WebformActions.WebformActionTypes.GET_WEBFORM_FIELDS_SUCCESS: {
      state.webformFields = action.payload;
      // console.log('state.webformFields: ', state.webformFields);

      return { ...state, loading: false };
    }
    case WebformActions.WebformActionTypes.GET_WEBFORM_FIELDS_ERROR: {
      return { ...state, loading: false };
    }

    case WebformActions.WebformActionTypes.GET_WEBFORM_ELEMENTS: {
      return { ...state, loading: true };
    }
    case WebformActions.WebformActionTypes.GET_WEBFORM_ELEMENTS_SUCCESS: {
      state.webformElements = action.payload;

      return { ...state, loading: false };
    }
    case WebformActions.WebformActionTypes.GET_WEBFORM_ELEMENTS_ERROR: {
      return { ...state, loading: false };
    }

    case WebformActions.WebformActionTypes.POST_WEBFORM: {
      const errObj: WebformError = {
        error: false,
        errorMessage: ''
      }
      state.success = '';

      return { ...state, loading: true, error: errObj  };
    }

    case WebformActions.WebformActionTypes.POST_WEBFORM_SUCCESS: {
      const errObj: WebformError = {
        error: false,
        errorMessage: ''
      }

      state.success = action.payload;

      return { ...state, loading: false, error: errObj};
    }

    case WebformActions.WebformActionTypes.POST_WEBFORM_ERROR: {
      const errObj: WebformError = {
        error: true,
        errorMessage: action.payload.error
      }

      return { ...state, loading: false, error: errObj  };
    }

    default:
      return state;
  }
}
