import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { flow, get, isEmpty, isNil, negate } from 'lodash/fp';
import {
  GetFeaturedLanguageResources,
  GetFeaturedLanguageResourcesError,
  GetFeaturedLanguageResourcesSuccess,
  GetFeaturedOrganizations,
  GetFeaturedOrganizationsError,
  GetFeaturedOrganizationsSuccess,
  GetFeaturedToolsAndServiceError,
  GetFeaturedToolsAndServiceSuccess,
  GetLastAdded,
  GetLastAddedError,
  GetLastAddedOrganizations,
  GetLastAddedOrganizationsError,
  GetLastAddedOrganizationsSuccess,
  GetLastAddedProjects,
  GetLastAddedProjectsError,
  GetLastAddedProjectsSuccess,
  GetLastAddedResource,
  GetLastAddedResourceError,
  GetLastAddedResourceSuccess,
  GetLastAddedSuccess,
  GetMostPopular,
  GetMostPopularError,
  GetMostPopularOrganizations,
  GetMostPopularOrganizationsError,
  GetMostPopularOrganizationsSuccess,
  GetMostPopularResource,
  GetMostPopularResourceError,
  GetMostPopularResourceSuccess,
  GetMostPopularSuccess,
  GetRecentlyUpdated,
  GetRecentlyUpdatedError,
  GetRecentlyUpdatedResource,
  GetRecentlyUpdatedResourceError,
  GetRecentlyUpdatedResourceSuccess,
  GetRecentlyUpdatedSuccess,
  GetSearchResults,
  GetSearchResultsError,
  GetSearchResultsNextPageSuccess,
  GetSearchResultsSuccess,
  GetTopCategoriesError,
  GetTopCategoriesSuccess,
  SearchResultTypes
} from './search.actions';
import {
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResults,
  SearchResultsCollection,
  TopCategories
} from '../../library/search/models/search-results.model';

import { ConfigService } from '@ngx-config/core';
import { EMPTY_UNIQUE_ID } from '../../framework/ngrx';
import { ERROR__NO_PAYLOAD } from '../../shared';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of as observableOf } from 'rxjs';

// import { SearchService } from './search.service';

@Injectable()
export class SearchEffects {
  @Effect() getJSON$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_SEARCH_RESULTS),
    map(get('payload')),
    switchMap(payload => {
      let url = `${this.config.getSettings('system.catalogue_url')}/repository/search/`;

      if (payload !== '') {
        url = `${url}?search=${payload}`;
      }

      url = url.replace('search=&', '&');
      // console.log('payload url: ', url);

      return this.http.get<SearchResults>(url).pipe(map(jsonRet => new GetSearchResultsSuccess(jsonRet)));
    })
  );

  @Effect() getSearchNextPage$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_SEARCH_RESULTS_NEXTPAGE),
    map(get('payload')),
    switchMap(payload => {
      const url = payload;

      return (
        this.http
          // .get<SearchResults>('../../../assets/data/demo-search-result.json')
          .get<SearchResults>(url)
          .pipe(map(jsonRet => new GetSearchResultsNextPageSuccess(jsonRet)))
      );
    })
  );

  @Effect() getTopCategories$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_TOP_CATEGORIES),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const tc = new TopCategories();
          tc.categories = jsonRet.facets._filter_function.function.buckets;

          if (jsonRet) {
            return new GetTopCategoriesSuccess(tc);
          } else {
            return new GetTopCategoriesError('error while getting top categories');
          }
        })
      );
    })
  );

  @Effect() getRecentrlyUpdatedTechnology$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_RECENTLY_UPDATED),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const rut = new RecentlyUpdatedTech();
          rut.results = jsonRet.results;

          if (jsonRet) {
            return new GetRecentlyUpdatedSuccess(rut);
          } else {
            return new GetRecentlyUpdatedError('error while getting recently updated technology');
          }
        })
      );
    })
  );

  @Effect() getMostPopularTechnology$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_MOST_POPULAR),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings('system.catalogue_url')}/registry/search/?resource_type__in=Tool/Service&ordering=-views`;
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new MostPopularTech();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetMostPopularSuccess(mpt);
          } else {
            return new GetMostPopularError('error while getting most popular technology');
          }
        })
      );
    })
  );

  @Effect() getMostPopularResource$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_MOST_POPULAR_RES),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__in=Lexical%2FConceptual+resource__Corpus__Language%20description&ordering=-views`;

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetMostPopularResourceSuccess(mpt);
          } else {
            return new GetMostPopularResourceError('error while getting most popular resources');
          }
        })
      );
    })
  );

  @Effect() getMostPopularOrganization$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings('system.catalogue_url')}/registry/search/?entity_type__term=Organization&ordering=-views`;

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetMostPopularOrganizationsSuccess(mpt);
          } else {
            return new GetMostPopularOrganizationsError('error while getting most popular organizations');
          }
        })
      );
    })
  );

  @Effect() getLatestAddedTechnology$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_LAST_ADDED),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date`;
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetLastAddedSuccess(mpt);
          } else {
            return new GetLastAddedError('error while getLatestAddedTechnology');
          }
        })
      );
    })
  );

  @Effect() GetFeaturedToolsAndService$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?format=json&ordering=-views&resource_type__in=Tool%2FService&featured__term=true`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const rut = new SearchResultsCollection();
          rut.results = jsonRet.results;

          if (jsonRet) {
            return new GetFeaturedToolsAndServiceSuccess(rut);
          } else {
            return new GetFeaturedToolsAndServiceError('error while GetFeaturedToolsAndService');
          }
        })
      );
    })
  );

  @Effect() GetFeaturedLanguageResources$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?format=json&ordering=-views&resource_type__in=Lexical%2FConceptual+resource__Corpus__Language%20description&featured__term=true`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const rut = new SearchResultsCollection();
          rut.results = jsonRet.results;

          if (jsonRet) {
            return new GetFeaturedLanguageResourcesSuccess(rut);
          } else {
            return new GetFeaturedLanguageResourcesError('error while GetFeaturedLanguageResources');
          }
        })
      );
    })
  );

  @Effect() GetFeaturedOrganizations$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_FEATURED_ORGANIZATIONS),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?format=json&ordering=-views&entity_type__term=Organization&featured__term=true`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const rut = new SearchResultsCollection();
          rut.results = jsonRet.results;

          if (jsonRet) {
            return new GetFeaturedOrganizationsSuccess(rut);
          } else {
            return new GetFeaturedOrganizationsError('error while GetFeaturedOrganizations');
          }
        })
      );
    })
  );

  @Effect() getRecentrlyUpdatedResources$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_RECENTLY_UPDATED_RES),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__in=Lexical%2FConceptual+resource__Corpus__Language%20description&ordering=-last_date_updated`;
      // /catalogue_backend/api/registry/search/?resource_type__term=Tool/Service&ordering=-last_date_updated
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-last_date_updated

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const rut = new SearchResultsCollection();
          rut.results = jsonRet.results;

          if (jsonRet) {
            return new GetRecentlyUpdatedResourceSuccess(rut);
          } else {
            return new GetRecentlyUpdatedResourceError('error while getRecentrlyUpdatedResources');
          }
        })
      );
    })
  );

  @Effect() getLatestAddedResources$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_LAST_ADDED_RES),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?resource_type__in=Lexical%2FConceptual+resource__Corpus__Language%20description&ordering=-creation_date`;
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetLastAddedResourceSuccess(mpt);
          } else {
            return new GetLastAddedResourceError('error while getLatestAddedResources');
          }
        })
      );
    })
  );

  @Effect() getLatestAddedProjects$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_LAST_ADDED_PROJECTS),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings('system.catalogue_url')}/registry/search/?entity_type__in=Project&ordering=-creation_date`;
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetLastAddedProjectsSuccess(mpt);
          } else {
            return new GetLastAddedProjectsError('error while getLatestAddedProjects');
          }
        })
      );
    })
  );

  @Effect() getLatestAddedOrganizations$ = this.actions$.pipe(
    ofType(SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS),
    // map(get('payload')),
    switchMap(() => {
      const url = `${this.config.getSettings(
        'system.catalogue_url'
      )}/registry/search/?entity_type__term=Organization&ordering=-creation_date `;
      // /catalogue_backend/api/registry/search/?resource_type__in=Tool/Service&ordering=-creation_date

      return this.http.get<SearchResults>(url).pipe(
        map((jsonRet: SearchResults) => {
          const mpt = new SearchResultsCollection();
          mpt.results = jsonRet.results;

          if (jsonRet) {
            return new GetLastAddedOrganizationsSuccess(mpt);
          } else {
            return new GetLastAddedOrganizationsError('error while getLatestAddedOrganizations');
          }
        })
      );
    })
  );

  constructor(private readonly actions$: Actions, private readonly http: HttpClient, private readonly config: ConfigService) {}
}

