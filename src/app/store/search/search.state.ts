import {
  FeaturedLanguageResource,
  FeaturedOrganizations,
  FeaturedToolsAndService,
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResults,
  SearchResultsCollection,
  TopCategories
} from '../../library/search/models/search-results.model';

export interface SearchState {
  searchResults: SearchResults;
  topCategories: TopCategories;
  featuredToolsAndService: FeaturedToolsAndService;
  recentlyUpdatedTech: RecentlyUpdatedTech;
  mostPopularTech: MostPopularTech;
  mostPopularResource: SearchResultsCollection;
  mostPopularOrganization: SearchResultsCollection;
  featuredLanguageResource: FeaturedLanguageResource;
  latestAddedTech: SearchResultsCollection;
  latestAddedRes: SearchResultsCollection;
  recentlyUpdatedRes: SearchResultsCollection;
  latestAddedProjects: SearchResultsCollection;
  latestAddedOrganizations: SearchResultsCollection;
  featuredOrganizations: FeaturedOrganizations;
  currentTSPageFeat: number;
  currentTSPage: number;
  currentTSPageMP: number;
  currentTSWidgetTab: number; // featured = 0 | most popular = 1 | recent = 2

  currentLRPageFeat: number;
  currentLRPage: number;
  currentLRPageMP: number;
  currentLRWidgetTab: number; // featured = 0 | most popular = 1 | recent = 2

  currentOPageFeat: number;
  currentOPage: number;
  currentOPageMP: number;
  currentOWidgetTab: number; // featured = 0 | most popular = 1 | recent = 2
  loading: boolean;
}

