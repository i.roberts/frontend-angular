import * as SearchActions from './search.actions';

/* eslint-disable complexity */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import {
  FeaturedLanguageResource,
  FeaturedOrganizations,
  FeaturedToolsAndService,
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResult,
  SearchResults,
  SearchResultsCollection,
  TopCategories
} from '../../library/search/models/search-results.model';

import { SearchState } from './search.state';

const defaultSearchState: SearchState = {
  searchResults: SearchResults.generateMockSearchResults(),
  topCategories: TopCategories.generateTopCategories(),
  featuredToolsAndService: FeaturedToolsAndService.generateFeaturedToolsAndService(),
  recentlyUpdatedTech: RecentlyUpdatedTech.generateRecentlyUpdatedTech(),
  mostPopularTech: MostPopularTech.generateMostPopularTech(),
  mostPopularResource: SearchResultsCollection.generateSearchResultsCollection(),
  mostPopularOrganization: SearchResultsCollection.generateSearchResultsCollection(),
  featuredLanguageResource: FeaturedLanguageResource.generateFeaturedToolsAndService(),
  latestAddedTech: SearchResultsCollection.generateSearchResultsCollection(),
  latestAddedRes: SearchResultsCollection.generateSearchResultsCollection(),
  recentlyUpdatedRes: SearchResultsCollection.generateSearchResultsCollection(),
  latestAddedProjects: SearchResultsCollection.generateSearchResultsCollection(),
  latestAddedOrganizations: SearchResultsCollection.generateSearchResultsCollection(),
  featuredOrganizations: FeaturedOrganizations.generateFeaturedOrganizations(),
  currentTSPageFeat: 0,
  currentTSPage: 0,
  currentTSPageMP: 0,
  currentTSWidgetTab: 0,

  currentLRPageFeat: 0,
  currentLRPage: 0,
  currentLRPageMP: 0,
  currentLRWidgetTab: 0,

  currentOPageFeat: 0,
  currentOPage: 0,
  currentOPageMP: 0,
  currentOWidgetTab: 0,

  loading: false
};

// tslint:disable-next-line:only-arrow-functions
export function SearchReducer(state: SearchState = defaultSearchState, action: any): SearchState {
  // console.log(action.type, action.payload, state);
  switch (action.type) {
    case SearchActions.SearchResultTypes.GET_SEARCH_RESULTS: {
      return { ...state, loading: true };
    }
    case SearchActions.SearchResultTypes.GET_SEARCH_RESULTS_SUCCESS: {
      state.searchResults = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_SEARCH_RESULTS_NEXTPAGE: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_SEARCH_RESULTS_NEXTPAGE_SUCCESS: {
      state.searchResults.results = [...state.searchResults.results, ...action.payload.results];
      state.searchResults.next = action.payload.next;

      return { ...state, loading: false };
    }

    /* SILO */
    case SearchActions.SearchResultTypes.GET_TOP_CATEGORIES: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_TOP_CATEGORIES_SUCCESS: {
      state.topCategories = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_TOP_CATEGORIES_ERROR: {
      state.topCategories = undefined;

      return { ...state, loading: false };
    }

    /* Featured tools and service */
    case SearchActions.SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE_SUCCESS: {
      state.featuredToolsAndService = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE_ERROR: {
      state.featuredToolsAndService = undefined;

      return { ...state, loading: false };
    }

    /* Featured language resources */
    case SearchActions.SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES_SUCCESS: {
      state.featuredLanguageResource = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES_ERROR: {
      state.featuredLanguageResource = undefined;

      return { ...state, loading: false };
    }

    /* Featured organizations */
    case SearchActions.SearchResultTypes.GET_FEATURED_ORGANIZATIONS: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_ORGANIZATIONS_SUCCESS: {
      state.featuredOrganizations = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_FEATURED_ORGANIZATIONS_ERROR: {
      state.featuredOrganizations = undefined;

      return { ...state, loading: false };
    }

    /* RECENTLY UPDATED */
    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED_SUCCESS: {
      state.recentlyUpdatedTech = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED_ERROR: {
      state.recentlyUpdatedTech = undefined;

      return { ...state, loading: false };
    }

    /* MOST POPULAR */
    case SearchActions.SearchResultTypes.GET_MOST_POPULAR: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_SUCCESS: {
      state.mostPopularTech = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_ERROR: {
      state.mostPopularTech = undefined;

      return { ...state, loading: false };
    }

    /* MOST POPULAR RES */
    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_RES: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_RES_SUCCESS: {
      state.mostPopularResource = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_RES_ERROR: {
      state.mostPopularResource = undefined;

      return { ...state, loading: false };
    }

    /* MOST POPULAR */
    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS_SUCCESS: {
      state.mostPopularOrganization = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS_ERROR: {
      state.mostPopularOrganization = undefined;

      return { ...state, loading: false };
    }

    /* LAST ADDED */
    case SearchActions.SearchResultTypes.GET_LAST_ADDED: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_SUCCESS: {
      state.latestAddedTech = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_ERROR: {
      state.latestAddedTech = undefined;

      return { ...state, loading: false };
    }

    /* RECENTLY UPDATED RESOURCE */
    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED_RES: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED_RES_SUCCESS: {
      state.recentlyUpdatedRes = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_RECENTLY_UPDATED_RES_ERROR: {
      state.recentlyUpdatedRes = undefined;

      return { ...state, loading: false };
    }

    /* LAST ADDED RESOURCE*/
    case SearchActions.SearchResultTypes.GET_LAST_ADDED_RES: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_RES_SUCCESS: {
      state.latestAddedRes = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_RES_ERROR: {
      state.latestAddedRes = undefined;

      return { ...state, loading: false };
    }

    /* LAST ADDED PROJECT */
    case SearchActions.SearchResultTypes.GET_LAST_ADDED_PROJECTS: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_PROJECTS_SUCCESS: {
      state.latestAddedProjects = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_PROJECTS_ERROR: {
      state.latestAddedProjects = undefined;

      return { ...state, loading: false };
    }

    /* LAST ADDED ORGANIZATION */
    case SearchActions.SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS: {
      return { ...state, loading: true };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS_SUCCESS: {
      state.latestAddedOrganizations = action.payload;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS_ERROR: {
      state.latestAddedOrganizations = undefined;

      return { ...state, loading: false };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_TSFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredToolsAndService.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPageFeat < totalPages ? state.currentTSPageFeat + 1 : 0;

      return { ...state, currentTSPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_TSFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredToolsAndService.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPageFeat > 0 ? state.currentTSPageFeat - 1 : totalPages;

      return { ...state, currentTSPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_LRFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredLanguageResource.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPageFeat < totalPages ? state.currentLRPageFeat + 1 : 0;

      return { ...state, currentLRPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_LRFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredLanguageResource.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPageFeat > 0 ? state.currentLRPageFeat - 1 : totalPages;

      return { ...state, currentLRPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_OFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredOrganizations.results.length / 5 - 1);
      const newCurrentPage = state.currentOPageFeat < totalPages ? state.currentOPageFeat + 1 : 0;

      return { ...state, currentLRPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_OFAV_PAGE: {
      const totalPages = Math.ceil(state.featuredOrganizations.results.length / 5 - 1);
      const newCurrentPage = state.currentOPageFeat > 0 ? state.currentOPageFeat - 1 : totalPages;

      return { ...state, currentLRPageFeat: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_TS_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedTech.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPage < totalPages ? state.currentTSPage + 1 : 0;

      return { ...state, currentTSPage: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_TS_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedTech.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPage > 0 ? state.currentTSPage - 1 : totalPages;

      return { ...state, currentTSPage: newCurrentPage };
    }
    case SearchActions.SearchResultTypes.SET_NEXT_TSMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularTech.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPageMP < totalPages ? state.currentTSPageMP + 1 : 0;

      return { ...state, currentTSPageMP: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_TSMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularTech.results.length / 5 - 1);
      const newCurrentPage = state.currentTSPageMP > 0 ? state.currentTSPageMP - 1 : totalPages;

      return { ...state, currentTSPageMP: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_WIDGET_TAB: {
      const widget = action.widget;
      const newTab = action.tab;

      if (widget === 'widget-services') {
        return { ...state, currentTSWidgetTab: newTab };
      } else if (widget === 'widget-lr') {
        return { ...state, currentLRWidgetTab: newTab };
      } else if (widget === 'widget-organizations') {
        return { ...state, currentOWidgetTab: newTab };
      } else {
        return { ...state };
      }
    }

    case SearchActions.SearchResultTypes.SET_NEXT_LR_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedRes.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPage < totalPages ? state.currentLRPage + 1 : 0;

      return { ...state, currentLRPage: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_LR_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedRes.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPage > 0 ? state.currentLRPage - 1 : totalPages;

      return { ...state, currentLRPage: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_LRMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularResource.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPageMP < totalPages ? state.currentLRPageMP + 1 : 0;

      return { ...state, currentLRPageMP: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_LRMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularResource.results.length / 5 - 1);
      const newCurrentPage = state.currentLRPageMP > 0 ? state.currentLRPageMP - 1 : totalPages;

      return { ...state, currentLRPageMP: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_O_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedRes.results.length / 5 - 1);
      const newCurrentPage = state.currentOPage < totalPages ? state.currentOPage + 1 : 0;

      return { ...state, currentOPage: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_O_PAGE: {
      const totalPages = Math.ceil(state.recentlyUpdatedRes.results.length / 5 - 1);
      const newCurrentPage = state.currentOPage > 0 ? state.currentOPage - 1 : totalPages;

      return { ...state, currentOPage: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_NEXT_OMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularOrganization.results.length / 5 - 1);
      const newCurrentPage = state.currentOPageMP < totalPages ? state.currentOPageMP + 1 : 0;

      return { ...state, currentOPageMP: newCurrentPage };
    }

    case SearchActions.SearchResultTypes.SET_PREV_OMP_PAGE: {
      const totalPages = Math.ceil(state.mostPopularOrganization.results.length / 5 - 1);
      const newCurrentPage = state.currentOPageMP > 0 ? state.currentOPageMP - 1 : totalPages;

      return { ...state, currentOPageMP: newCurrentPage };
    }

    default:
      return state;
  }
}

