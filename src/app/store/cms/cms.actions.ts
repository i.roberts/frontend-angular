import { Action } from '@ngrx/store';

export enum CmsActionTypes {
  GET_CMS_PAGE = '[Cms] GET_CMS_PAGE',
  GET_CMS_PAGE_SUCCESS = '[Cms] GET_CMS_PAGE_SUCCESS',
  GET_CMS_PAGE_ERROR = '[Cms] GET_CMS_PAGE_ERROR',
  SET_CMS_SERVER_PAGE = '[Cms] SET_CMS_SERVER_PAGE',
  GET_NEWS_PAGE = '[Cms] GET_NEWS_PAGE',
  GET_NEWS_PAGE_SUCCESS = '[Cms] GET_NEWS_PAGE_SUCCESS',
  GET_CATALOGUE_STATISTICS = '[Cms] GET_CATALOGUE_STATISTICS',
  GET_CATALOGUE_STATISTICS_SUCCESS = '[Cms] GET_CATALOGUE_STATISTICS_SUCCESS',

}

export class GetCmsPage implements Action {
  readonly type = CmsActionTypes.GET_CMS_PAGE;

  constructor(public alias: string) {}
}

export class GetCmsPageSuccess implements Action {
  readonly type = CmsActionTypes.GET_CMS_PAGE_SUCCESS;

  constructor(public payload: any) {}
}

export class GetCmsPageError implements Action {
  readonly type = CmsActionTypes.GET_CMS_PAGE_ERROR;

  constructor(public payload: string) {}
}

export class SaveServerData implements Action {
  readonly type = CmsActionTypes.SET_CMS_SERVER_PAGE;

  constructor(public cmsData: any) {}
}

export class GetNewsPage implements Action {
  readonly type = CmsActionTypes.GET_NEWS_PAGE;

  constructor(public pagenumber: string) {}
}

export class GetNewsPageSuccess implements Action {
  readonly type = CmsActionTypes.GET_NEWS_PAGE_SUCCESS;

  constructor(public payload: any) {}
}

export class GetCatalogueStatistics implements Action {
  readonly type = CmsActionTypes.GET_CATALOGUE_STATISTICS;

  constructor(public catalogueUrl: any) {}
}

export class GetCatalogueStatisticsSuccess implements Action {
  readonly type = CmsActionTypes.GET_CATALOGUE_STATISTICS_SUCCESS;

  constructor(public payload: any) {}
}

export type Actions =
  | GetCmsPage
  | GetCmsPageSuccess
  | GetCmsPageError
  | SaveServerData
  | GetNewsPage
  | GetNewsPageSuccess
  | GetCatalogueStatistics
  | GetCatalogueStatisticsSuccess;
