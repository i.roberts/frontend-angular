import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { TransferStateService } from '../../transfer-state.service';
import { get } from 'lodash/fp';
import { Observable, of as observableOf } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CmsService } from '../../+cms/cms.service';

import { SaveServerData, CmsActionTypes, GetNewsPageSuccess, GetCatalogueStatisticsSuccess } from './cms.actions';


const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class CmsEffects {

  @Effect() getCms$: Observable<Action> = this.actions$.pipe(
    ofType(CmsActionTypes.GET_CMS_PAGE),
    map(get('alias')),
    // eslint-disable-next-line arrow-body-style
    switchMap(alias =>{


      return this.transferStateService.fetch(alias, this.cmsService.subCmsPageContent(alias)).pipe(map(jsonRet => new SaveServerData(jsonRet)))

    })

  );

  @Effect() getCmsNews$: Observable<Action> = this.actions$.pipe(
    ofType(CmsActionTypes.GET_NEWS_PAGE),
    map(get('pagenumber')),
    // eslint-disable-next-line arrow-body-style
    switchMap(pagenumber =>{

      return this.transferStateService.fetch('news', this.cmsService.subCmsPageContent('news', pagenumber)).pipe(map(jsonRet => new GetNewsPageSuccess(jsonRet)))

    })

  );

  @Effect() getCatalogueStatistics$: Observable<Action> = this.actions$.pipe(
    ofType(CmsActionTypes.GET_CATALOGUE_STATISTICS),
    map(get('catalogueUrl')),
    // eslint-disable-next-line arrow-body-style
    switchMap(catalogueUrl =>{

      return this.transferStateService.fetch('statistics', this.cmsService.getCatalogueStatistics(catalogueUrl)).pipe(map(jsonRet => new GetCatalogueStatisticsSuccess(jsonRet)))

    })

  );

  skipjWT = new HttpHeaders().set(InterceptorSkipHeader, '');

  constructor(private readonly actions$: Actions, private readonly http: HttpClient, private readonly config: ConfigService, private readonly cmsService: CmsService, private readonly transferStateService: TransferStateService) {}
}
