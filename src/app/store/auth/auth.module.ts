import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';

import { AuthEffects } from './auth.effects';
import * as fromAuth from './auth.reducer';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('auth', fromAuth.AuthReducer, {}), EffectsModule.forFeature([AuthEffects])],
  providers: []
})
export class AuthModule {}
