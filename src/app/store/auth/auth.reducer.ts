import { Auth } from '../../library/auth/auth.model';

import * as AuthActions from './auth.actions';
import { AuthState } from './auth.state';

const defaultAuthState: AuthState = {
  isAuthenticated: Auth.generateMockAuth(),
  idToken: Auth.generateMockIDToken(),
  userProfile: Auth.generateMockUserProfile()
};

// tslint:disable-next-line:only-arrow-functions
export function AuthReducer(state: AuthState = defaultAuthState, action: any): AuthState {
  switch (action.type) {
    case AuthActions.AuthTypes.SET_LOGIN_STATE: {
      return { ...state, isAuthenticated: action.isLoggedIn };
    }

    case AuthActions.AuthTypes.SET_ID_TOKEN: {
      return { ...state, idToken: action.idToken };
    }

    case AuthActions.AuthTypes.SET_USER_PROFILE: {
      return { ...state, userProfile: action.userProfile };
    }

    default:
      return state;
  }
}
