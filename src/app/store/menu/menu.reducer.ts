import { CmsMenuTree, Menu } from '../../library/menu/menu.model';

import * as MenuActions from './menu.actions';
import { MenuState } from './menu.state';

const defaultMenuState: MenuState = {
  menu: Menu.generateMockMenu(),
  loading: false,
  currentRoute: '',
  footerMenu: Menu.generateMockFooterMenu(),
  feedbackMenu: Menu.generateMockFeedbackMenu()
};

// tslint:disable-next-line:only-arrow-functions
export function MenuReducer(state: MenuState = defaultMenuState, action: any): MenuState {
  switch (action.type) {
    case MenuActions.MenuTypes.GET_MENU: {
      return { ...state, loading: true };
    }
    case MenuActions.MenuTypes.GET_MENU_SUCCESS: {
      state.menu = action.payload;

      return { ...state, loading: false };
    }
    case MenuActions.MenuTypes.GET_FOOTER_MENU: {
      return { ...state, loading: true };
    }
    case MenuActions.MenuTypes.GET_FOOTER_MENU_SUCCESS: {
      state.footerMenu = action.payload;

      return { ...state, loading: false };
    }

    case MenuActions.MenuTypes.GET_FEEDBACK_MENU: {
      return { ...state, loading: true };
    }
    case MenuActions.MenuTypes.GET_FEEDBACK_MENU_SUCCESS: {
      state.feedbackMenu = action.payload;

      return { ...state, loading: false };
    }

    case MenuActions.MenuTypes.SET_CURRENT_ROUTE: {
      state.currentRoute = action.payload;

      return { ...state, loading: false };
    }

    case MenuActions.MenuTypes.CURRENT_ROUTE_SET: {
      return { ...state, loading: false };
    }

    case MenuActions.MenuTypes.GET_CURRENT_ROUTE: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
