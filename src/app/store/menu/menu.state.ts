import { CmsMenuTree, CmsMenuTreeItem, CmsSubmenuTree } from '../../library/menu/menu.model';

export interface MenuState {
  menu: Array<CmsMenuTreeItem>;
  loading: boolean;
  currentRoute: string;
  footerMenu: Array<CmsMenuTreeItem>;
  feedbackMenu: Array<CmsMenuTreeItem>;
}
