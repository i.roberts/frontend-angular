import { Action } from '@ngrx/store';

import { CmsMenuTree, CmsSubmenuTree } from '../../library/menu/menu.model';

export enum MenuTypes {
  GET_MENU = '[Menu] GET_MENU',
  GET_MENU_SUCCESS = '[Menu] GET_MENU_SUCCESS',
  GET_MENU_ERROR = '[Menu] GET_MENU_ERROR',
  GET_FOOTER_MENU = '[Menu] GET_FOOTER_MENU',
  GET_FOOTER_MENU_SUCCESS = '[Menu] GET_FOOTER_MENU_SUCCESS',
  GET_FEEDBACK_MENU = '[Menu] GET_FEEDBACK_MENU',
  GET_FEEDBACK_MENU_SUCCESS = '[Menu] GET_FEEDBACK_MENU_SUCCESS',
  SET_CURRENT_ROUTE = '[Menu] SET_CURRENT_ROUTE',
  GET_CURRENT_ROUTE = '[Menu] GET_CURRENT_ROUTE',
  CURRENT_ROUTE_SET = '[Menu] CURRENT_ROUTE_SET'
}

export class GetMenu implements Action {
  readonly type = MenuTypes.GET_MENU;

  constructor(public payload: string) {}
}

export class GetMenuSuccess implements Action {
  readonly type = MenuTypes.GET_MENU_SUCCESS;

  constructor(public payload: CmsMenuTree) {}
}

export class GetFooterMenu implements Action {
  readonly type = MenuTypes.GET_FOOTER_MENU;

  constructor(public payload: string) {}
}

export class GetFooterMenuSuccess implements Action {
  readonly type = MenuTypes.GET_FOOTER_MENU_SUCCESS;

  constructor(public payload: CmsMenuTree) {}
}

export class GetFeedbackMenu implements Action {
  readonly type = MenuTypes.GET_FEEDBACK_MENU;

  constructor(public payload: string) {}
}

export class GetFeedbackMenuSuccess implements Action {
  readonly type = MenuTypes.GET_FEEDBACK_MENU_SUCCESS;

  constructor(public payload: CmsMenuTree) {}
}

export class GetMenuError implements Action {
  readonly type = MenuTypes.GET_MENU_ERROR;
}

// CURRENT ROUTE
export class SetCurrentRoute implements Action {
  readonly type = MenuTypes.SET_CURRENT_ROUTE;

  constructor(public payload: string) {}
}

export class CurrentRouteSet implements Action {
  readonly type = MenuTypes.CURRENT_ROUTE_SET;
}

export class GetCurrentRoute implements Action {
  readonly type = MenuTypes.GET_CURRENT_ROUTE;
}

export type Actions =
  | GetMenu
  | GetMenuSuccess
  | GetMenuError
  | SetCurrentRoute
  | GetCurrentRoute
  | CurrentRouteSet
  | GetFooterMenu
  | GetFooterMenuSuccess
  | GetFeedbackMenu
  | GetFeedbackMenuSuccess;
