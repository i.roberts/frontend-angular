import { FrameworkState } from '../framework/store';
import { AuthState } from './auth/auth.state';

import { MenuState } from './menu/menu.state';
import { SearchState } from './search/search.state';
import { WebformState } from './webforms/webform.state';
import { CmsState } from './cms/cms.state';

export interface State extends FrameworkState {
  search: SearchState;
  menu: MenuState;
  webforms: WebformState;
  auth: AuthState,
  cms: CmsState
}
