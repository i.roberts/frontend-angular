export class MetadataRecord {
  id: number;
  // tslint:disable-next-line:variable-name
  metadata_header: {
    id: number;
    metadata_record_identifier: {
      id: number;
      scheme_name: string;
      scheme_uri: string;
      value: string;
    };
    metadata_creators: Array<MetadataCreator>;
    source_of_metadata_record: {
      id: number;
      collected_from: {
        id: number;
        identifiers: Array<number>;
        name: {
          en: string;
        };
      };
      source_metadata_link: string;
    };
    metadata_languages: Array<string>;
    metadata_creation_date: string;
    revision: string;
  };
  // tslint:disable-next-line:variable-name
  common_info: {
    id: number;
    identification: {
      id: number;
      resource_identifiers: Array<ResourceIdentifier>;
      resource_name: {
        en: string;
      };
      description: {
        en: string;
      };
      resource_short_name: {
        en: string;
      };
      logo: string;
    };
    version: {
      id: number;
      version: string;
      digest: string | null;
      date: string | null;
      type: string | null;
      revision: string | null;
    };
    contact: {
      id: number;
      contact_persons: Array<ContactPerson>;
      contact_organizations: Array<string>;
      contact_groups: Array<string>;
      mailing_lists: Array<string>;
      contact_point: string;
      type: string;
    };
    resource_rights: {
      id: number;
      rights_holders: Array<RightsHolder>;
      rights_statement: string;
      copyright_statement: string | null;
    };
    documentations: Array<Documentation>;
    domains: Array<Domain>;
    relations: Array<Relation>;
    keywords: Array<string> | null;
  };

  // tslint:disable-next-line:variable-name
  language_resource: {
    id: number;
    function_info: {
      id: number;
      function: Array<string>;
      function_other: string | null;
    };
    distributions: Array<Distribution>;
    dataset_distribution: DatasetDistribution;
    input_content_resources: Array<InputContentResource>;
    output_resources: Array<OutputResource>;
    dependencies: Array<string>;
    resource_type: string;
    creation: string | null;
  };
  // tslint:disable-next-line:variable-name
  physical_resources: string | null;
  // tslint:disable-next-line:variable-name
  storage_object: number;
  owners: Array<number>;

  
  constructor() {
    this.id = 0;
    this.metadata_header = {
      id: 0,
      metadata_record_identifier: {
        id: 0,
        scheme_name: '',
        scheme_uri: '',
        value: ''
      },
      metadata_creators: [],
      source_of_metadata_record: {
        id: 0,
        collected_from: {
          id: 0,
          identifiers: [],
          name: {
            en: ''
          }
        },
        source_metadata_link: ''
      },
      metadata_languages: [],
      metadata_creation_date: '',
      revision: ''
    };

    this.common_info = {
      id: 0,
      identification: {
        id: 0,
        resource_identifiers: [],
        resource_name: {
          en: ''
        },
        description: {
          en: ''
        },
        resource_short_name: {
          en: ''
        },
        logo: ''
      },
      version: {
        id: 0,
        version: '',
        digest: undefined,
        date: undefined,
        type: undefined,
        revision: undefined
      },
      contact: {
        id: 1,
        contact_persons: [],
        contact_organizations: [],
        contact_groups: [],
        mailing_lists: [],
        contact_point: '',
        type: ''
      },
      resource_rights: {
        id: 0,
        rights_holders: [],
        rights_statement: '',
        copyright_statement: undefined
      },
      documentations: [],
      domains: [],
      relations: [],
      keywords: undefined
    };

    this.language_resource = {
      id: 0,
      function_info: {
        id: 4,
        function: [],
        function_other: undefined
      },
      distributions: [
        {
          id: 0,
          is_described_by: undefined,
          distribution_rights: {
            id: 0,
            licences: [],
            rights_statement: '',
            attribution_text: '',
            availability_start_date: '',
            availability_end_date: ''
          },
          distribution_form: '',
          digest: undefined,
          follows_elg_specs: true,
          distribution_location: '',
          execution_location: undefined,
          elg_execution_location: '',
          download_location: '',
          demo_location: '',
          openAPI_spec_url: undefined,
          gui_url: undefined,
          elg_demo_location: undefined,
          additional_hw_requirements: undefined,
          command: '',
          web_service_type: '',
          operating_system: []
        }
      ],
      dataset_distribution: {
        distribution_rights: {
          id: 0,
          licences: [],
          rights_statement: '',
          attribution_text: '',
          availability_start_date: '',
          availability_end_date: ''
        }
      }
      ,
      input_content_resources: [
        {
          id: 0,
          data_formats: [],
          languages: [],
          annotation_types: [],
          type_system: undefined,
          annotation_schema: undefined,
          annotation_resource: undefined,
          resource_type: '',
          character_encodings: [],
          samples_location: undefined
        }
      ],
      output_resources: [
        {
          id: 0,
          data_formats: [],
          languages: [],
          annotation_types: [],
          type_system: undefined,
          annotation_schema: undefined,
          annotation_resource: undefined,
          resource_type: '',
          character_encodings: [],
          samples_location: undefined
        }
      ],
      dependencies: [],
      resource_type: '',
      creation: undefined
    };
    this.physical_resources = undefined;
    this.storage_object = 0;
    this.owners = [];
  }

  static generateMockCatalogueDetailsResults(): MetadataRecord {
    return {
      id: 0,
      metadata_header: {
        id: 0,
        metadata_record_identifier: {
          id: 0,
          scheme_name: '',
          scheme_uri: '',
          value: ''
        },
        metadata_creators: [],
        source_of_metadata_record: {
          id: 0,
          collected_from: {
            id: 0,
            identifiers: [],
            name: {
              en: ''
            }
          },
          source_metadata_link: ''
        },
        metadata_languages: [],
        metadata_creation_date: '',
        revision: ''
      },
      common_info: {
        id: 0,
        identification: {
          id: 0,
          resource_identifiers: [],
          resource_name: {
            en: ''
          },
          description: {
            en: ''
          },
          resource_short_name: {
            en: ''
          },
          logo: ''
        },
        version: {
          id: 0,
          version: '',
          digest: undefined,
          date: undefined,
          type: undefined,
          revision: undefined
        },
        contact: {
          id: 1,
          contact_persons: [],
          contact_organizations: [],
          contact_groups: [],
          mailing_lists: [],
          contact_point: '',
          type: ''
        },
        resource_rights: {
          id: 0,
          rights_holders: [],
          rights_statement: '',
          copyright_statement: undefined
        },
        documentations: [],
        domains: [],
        relations: [],
        keywords: undefined
      },
      language_resource: {
        id: 0,
        function_info: {
          id: 4,
          function: [],
          function_other: undefined
        },
        distributions: [
          {
            id: 0,
            is_described_by: undefined,
            distribution_rights: {
              id: 0,
              licences: [],
              rights_statement: '',
              attribution_text: '',
              availability_start_date: '',
              availability_end_date: ''
            },
            distribution_form: '',
            digest: undefined,
            follows_elg_specs: true,
            distribution_location: '',
            execution_location: undefined,
            elg_execution_location: '',
            download_location: '',
            demo_location: '',
            openAPI_spec_url: undefined,
            gui_url: undefined,
            elg_demo_location: undefined,
            additional_hw_requirements: undefined,
            command: '',
            web_service_type: '',
            operating_system: []
          }
        ],
        dataset_distribution: {
          distribution_rights: {
            id: 0,
            licences: [],
            rights_statement: '',
            attribution_text: '',
            availability_start_date: '',
            availability_end_date: ''
          }
        }, 
        input_content_resources: [
          {
            id: 0,
            data_formats: [],
            languages: [],
            annotation_types: [],
            type_system: undefined,
            annotation_schema: undefined,
            annotation_resource: undefined,
            resource_type: '',
            character_encodings: [],
            samples_location: undefined
          }
        ],
        output_resources: [
          {
            id: 0,
            data_formats: [],
            languages: [],
            annotation_types: [],
            type_system: undefined,
            annotation_schema: undefined,
            annotation_resource: undefined,
            resource_type: '',
            character_encodings: [],
            samples_location: undefined
          }
        ],
        dependencies: [],
        resource_type: '',
        creation: undefined
      },
      physical_resources: undefined,
      storage_object: 0,
      owners: []
    };
  }

}

export interface MetadataCreator {
  id: number;
  identifiers: Array<number>;
  type: string;
  name: {
    en: string;
  };
}

export interface ResourceIdentifier {
  id: number;
  scheme_name: string;
  scheme_uri: string;
  value: string;
}

export interface ContactPerson {
  id: number;
  identifiers: Array<number>;
  type: string;
  name: {
    en: string;
  };
}

export interface RightsHolder {
  id: number;
  identifiers: Array<Identifier>;
  type: string;
  name: {
    en: string;
  };
}

export interface Documentation {
  id: 1;
  publication_identifiers: Array<PublicationIdentifier>;
  documentation_type: string;
  document_bibtex: string;
  documentation_bibliographic_data: string;
}

export interface PublicationIdentifier {
  id: number;
  scheme_name: string;
  scheme_uri: string;
  value: string;
}

export interface Domain {
  id: number;
  scheme_name: string;
  scheme_uri: string;
  value: string;
}

export interface Relation {
  id: number;
  related_resource: {
    id: number;
    identifiers: Array<number>;
    name: {
      en: string;
    };
    tool_service_creation: string | null;
  };
  relation_type: string;
}

export interface TechnicalPart {
  id: number;
  sizes: Array<Size>;
  text_formats: Array<TextFormats>;
  character_encodings: Array<CharacterEncodings>;
  media_type: string;
}

export interface Distribution {
  id: number;
  is_described_by: string | null;
  // technical_parts: Array<TechnicalPart>;
  distribution_rights: {
    id: number;
    licences: Array<Licence>;
    rights_statement: string;
    attribution_text: string;
    availability_start_date: string;
    availability_end_date: string;
  };
  distribution_form: string;
  digest: string | null;
  follows_elg_specs: boolean;
  distribution_location: string;
  execution_location: string | null;
  elg_execution_location: string | null;
  download_location: string;
  demo_location: string;
  openAPI_spec_url: string | null;
  gui_url: string | null;
  elg_demo_location: string;
  additional_hw_requirements: string;
  command: string;
  web_service_type: string;
  operating_system: Array<string>;
}

export interface DatasetDistribution {
  distribution_rights: {
    id: number;
    licences: Array<Licence>;
    rights_statement: string;
    attribution_text: string;
    availability_start_date: string;
    availability_end_date: string;
  };
}

export interface InputContentResource {
  id: number;
  data_formats: Array<DataFormat>;
  languages: Array<Language>;
  annotation_types: Array<AnnotationType>;
  type_system: AnnotationInfo;
  annotation_schema: AnnotationInfo;
  annotation_resource: AnnotationInfo;
  resource_type: string;
  character_encodings: Array<string>;
  samples_location: string | null;
}

export interface OutputResource {
  id: number;
  data_formats: Array<DataFormat>;
  languages: Array<Language>;
  annotation_types: Array<AnnotationType>;
  type_system: AnnotationInfo;
  annotation_schema: AnnotationInfo;
  annotation_resource: AnnotationInfo;
  resource_type: string;
  character_encodings: Array<string>;
  samples_location: string | null;
}

export interface DataFormat {
  id: number;
  data_format: string;
  data_format_other: string | null;
}

export interface Size {
  id: number;
  size: string;
  size_unit: string;
}

export interface TextFormats {
  id: number;
  data_format: {
    id: number;
    data_format: string;
    data_format_other: string;
  };
  size: Size;
}

export interface CharacterEncodings {
  id: number;
  size: Size;
  encoding: string;
}

export interface Licence {
  id: number;
  identifiers: Array<Identifier>;
  name: {
    en: string;
  } | null ;
}

export interface Identifier {
  id: number;
  scheme_name: string;
  scheme_uri: string | null;
  value: string;
}

export interface Language {
  id: number;
  language_tag: string;
  language_id: string;
  script_id: number;
  region_id: number;
  variant_id: Array<number>;
}

export interface LanguageVarieties {
  id: number;
  size: Size;
  type: string;
  name: string;
}

export interface TimeClassification {
  id: number;
  size: Size;
  time_coverage: string;
}

export interface GeographicClassification {
  id: number;
  size: Size | null;
  geographic_coverage: string;
}

export interface CorpusPart {
  id: number;
  creation: {
    id: number;
    original_sources: Array<number>;
    created_by: Array<string>;
    creation_mode: string;
    creation_mode_details: string;
  };
  modality: {
    id: number;
    size: Size | null;
    type: Array<string>;
    details: string;
  };
  text_classifications: Array<TextClassification>;
  annotations: Array<Annotation>;
  media_type: string;
}

export interface TextClassification {
  id: number;
  text_genre: GenClassification;
  text_type: GenClassification;
  register: GenClassification;
  subject: GenClassification;
  size: Size;
  keywords: Array<string>;
}

export interface GenClassification {
  id: number;
  classification_scheme_name: string;
  scheme_uri: string;
}

export interface Annotation {
  id: number;
  annotation_types: Array<AnnotationType>;
  type_system: AnnotationInfo;
  annotation_schema: AnnotationInfo;
  annotation_resource: AnnotationInfo;
  guidelines_documented_in: Array<GuidelineDocumentedIn>;
  is_annotated_by: Array<AnnotationInfo>;
  annotation_date: {
    id: number;
    date: {
      id: number;
      day: number;
      month: number;
      year: number;
    };
    date_range: string | null;
  };
  annotators: Array<Annotator>;
  size: Size;
  annotation_mode: string;
  annotation_mode_details: string;
  interannotator_agreement: string | null;
  intraannotator_agreement: string | null;
}

export interface AnnotationType {
  id: number;
  annotation_type: string;
  annotation_type_other: string;
}

export interface AnnotationInfo {
  id: number;
  identifiers: Array<number>;
  name: {
    en: string;
  };
  tool_service_creation: string | null;
}

export interface GuidelineDocumentedIn {
  id: number;
  publication_identifiers: Array<number>;
  document_unstructured: string;
}

export interface Annotator {
  id: number;
  identifiers: Array<Identifier>;
  type: string;
  name: {
    en: string;
  };
}
