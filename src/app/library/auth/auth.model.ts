export class Auth {
  isAuthenticated: boolean;
  idToken: any;
  userProfile: any;

  constructor() {
    this.isAuthenticated = false;
    this.idToken = {};
    this.userProfile = {};
  }

  static generateMockAuth(): boolean {
    return false;
  }

  static generateMockIDToken(): any {
    return {};
  }

  static generateMockUserProfile(): any {
    return {};
  }
}
