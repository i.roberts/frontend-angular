import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule, makeStateKey } from '@angular/platform-browser';
import { configFactory, CoreModule, metaFactory, SharedModule } from './framework/core';
import { ConfigLoader, ConfigService } from '@ngx-config/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/* eslint-disable @typescript-eslint/tslint/config */
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { I18NModule, translateFactory } from './framework/i18n';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { TranslateLoader, TranslateService } from '@ngx-translate/core';

import { AnalyticsModule } from './framework/analytics';
import { ANGULARTICS2_TOKEN } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { AppComponent } from './app.component';
import { CmsFeatureCheckRouteModule } from './check-route/cms-feature-check-route.module';
import { CmsModule } from './+cms/cms.module';
import { DomService } from './+cms/dom.service';
import { DynamicModule } from './+cms/dynamiccontent/dynamic.module';
import { FootComponent } from './layout/foot/foot.component';
import { HeaderComponent } from './layout/header.component';
import { HttpInterceptorModule } from './framework/http';
import { MaterialModule } from './framework/material';
import { MetaLoader } from '@ngx-meta/core';
import { routes } from './app.routes';
import { SearchModule } from './search/search.module';
import { StoreModule } from './store';
import { TokenInterceptor } from './shared/basic.interceptor';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { TransferStateService } from './transfer-state.service';

export const REQ_KEY = makeStateKey<string>('req');

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app-id' }),
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    CmsFeatureCheckRouteModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      preloadingStrategy: PreloadAllModules,
      scrollOffset: [0, 64]
    }),
    AnalyticsModule.forRoot([
      {
        provide: ANGULARTICS2_TOKEN,
        useValue: {
          providers: [Angulartics2GoogleAnalytics],
          settings: {}
        }
      }
    ]),
    CoreModule.forRoot([
      {
        provide: ConfigLoader,
        useFactory: configFactory,
        deps: [Injector]
      },
      {
        provide: MetaLoader,
        useFactory: metaFactory,
        deps: [ConfigService, TranslateService]
      }
    ]),
    SharedModule,
    HttpInterceptorModule,
    I18NModule.forRoot([
      {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [HttpClient]
      }
    ]),
    MaterialModule,
    StoreModule.forRoot(),
    DynamicModule,
    FormsModule,
    ReactiveFormsModule,
    SearchModule,
    CmsModule
  ],
  declarations: [FootComponent, HeaderComponent, AppComponent],
  providers: [
    TransferStateService,
    DomService,
    // I18N_ROUTER_PROVIDERS
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class AppModule {}

