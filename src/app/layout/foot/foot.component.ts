import { isPlatformBrowser } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnInit, PLATFORM_ID, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Language, LanguageSelectors, State } from '../../store';
import { MenuState } from '../../store/menu/menu.state';
import { BaseComponent } from '../../framework/core';
import { CookieService } from './cookie.service';

import { CmsMenuService } from '../../+cms/cms-menu.service';
import { CmsMenuTree, CmsMenuTreeItem } from '../../library/menu/menu.model';
import * as MenuAction from '../../store/menu/menu.actions';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { CheckRouteService } from '../../check-route/check-route.service';

@Component({
  selector: 'app-foot',
  templateUrl: './foot.component.html',
  styleUrls: ['./foot.component.scss'],
  providers: [CmsMenuService],
  encapsulation: ViewEncapsulation.Emulated
})
export class FootComponent extends BaseComponent implements OnInit {
  currentLanguage$: Observable<Language>;
  availableLanguages: Array<Language>;
  footItems: Array<CmsMenuTreeItem>;
  feedbackItems: Array<CmsMenuTreeItem>;
  year: number = new Date().getFullYear();
  showCookieBanner: boolean;
  selectedLanguage: string;

  constructor(
    public router: Router,
    private readonly store$: Store<State>,
    private readonly cd: ChangeDetectorRef,
    private readonly config: ConfigService,
    private readonly menuStore: Store<MenuState>,
    private readonly cookieService: CookieService,
    private readonly checkRouteService: CheckRouteService,
    private readonly angulartics2GoogleTagManager: Angulartics2GoogleTagManager,
    public translate: TranslateService,
    @Inject(PLATFORM_ID) private readonly platformId: Object
  ) {
    super();
    this.showCookieBanner = false;

  }

  ngOnInit(): void {
    this.store$.pipe(select(LanguageSelectors.getWorkingLanguage)).subscribe(currentLang => {
      if (currentLang !== undefined) {
        this.selectedLanguage = currentLang.code;
      }
    });
    this.availableLanguages = this.config.getSettings('i18n.availableLanguages');

    this.menuStore.dispatch(new MenuAction.GetFooterMenu('footer'));
    this.menuStore.dispatch(new MenuAction.GetFeedbackMenu('feedback'));

    this.menuStore
      .select('menu')
      .pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).footerMenu),
        distinctUntilChanged()
      )
      .subscribe(menuData => {
        this.footItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;
        this.importRoutes(this.footItems);
        this.cd.markForCheck();
      });

    this.menuStore
      .select('menu')
      .pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).feedbackMenu),
        distinctUntilChanged()
      )
      .subscribe(menuData => {
        this.feedbackItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;
        this.importRoutes(this.feedbackItems);
        this.cd.markForCheck();
      });

      if (isPlatformBrowser(this.platformId) && !this.cookieService.getCookie("cookieconsent_status")){
        this.showCookieBanner = true;
    } else if (isPlatformBrowser(this.platformId) && this.cookieService.getCookie("cookieconsent_status") === "allow") {
      this.angulartics2GoogleTagManager.startTracking();
    } else if (isPlatformBrowser(this.platformId) && this.cookieService.getCookie("cookieconsent_status") === "deny") {
      this.cookieService.deleteCookie("_ga");
      this.cookieService.deleteCookie("_gid");
    }


  }

  getRoute(event: any): void {
    const goRoute = event.target.getAttribute('data-link');
    if (goRoute !== undefined) {
      this.router.navigate([goRoute])
      .catch(err => {console.log(err)})
      .then(() => {console.log('this will succeed')})
      .catch(() => 'obligatory catch');
    }
  }

  importRoutes(menuItems: Array<CmsMenuTreeItem>): void {
    if (menuItems && menuItems.length > 0) {
      for (const item of menuItems) {
        if(item.field_elg_menu_link_type === 'React') {
          this.checkRouteService.addRoute(`/catalogue${  item.relative.replace('/cms/', '')}`);
        } else if(item.field_elg_menu_link_type === 'External') {
          // there is no need to add router path for external resource
        } else {
          this.checkRouteService.addRoute(item.relative.replace('/cms/', ''));
        }
      }
    }

  }

  setCookie(): void{
    this.cookieService.setCookie("cookieconsent_status", "allow", 365);
    this.showCookieBanner = false;
  }

  refuseCookie($event: Event): void{
    $event.preventDefault();
    this.cookieService.setCookie("cookieconsent_status", "deny", 365);
    this.showCookieBanner = false;
  }

}
