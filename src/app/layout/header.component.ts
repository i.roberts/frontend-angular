import { ChangeDetectorRef, Component, Inject, OnInit, PLATFORM_ID, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map, filter } from 'rxjs/operators';

import { BaseComponent } from '../framework/core';
import { KeycloakService } from 'keycloak-angular';
import { Store } from '@ngrx/store';

import * as MenuAction from '../store/menu/menu.actions';
import { CmsMenuTree, CmsMenuTreeItem, CmsSubmenuTree, CmsMenuOption } from '../library/menu/menu.model';
import { CmsMenuService } from '../+cms/cms-menu.service';
import { MenuState } from '../store/menu/menu.state';
import { State } from '../store/state';
import { AuthState } from '../store/auth/auth.state';
import { CheckRouteService } from '../check-route/check-route.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['header.component.scss'],
  providers: [CmsMenuService]
  // TODO: maintain immutability
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent extends BaseComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();

  baseUrl: string;
  title: string;
  siteMenuItems: Array<CmsMenuTreeItem>;
  externalMenuAvailable: boolean;
  menu: Observable<CmsMenuTree>;
  cmsMenu: CmsMenuTree;
  acitveMenuItem: string;
  acitveMenuSubItem: string;
  acitveMenuLVL3Item: string;
  // keycloakService = undefined;
  submenus: Array<CmsSubmenuTree>;
  hasSubmenus = false;
  hasLVL3Submenus = false;
  currentSubmenu: Array<CmsMenuTreeItem>;
  currentSubmenuLVL3: Array<CmsMenuTreeItem>;
  isSubmenu = false;
  parentLink = '';
  isAuthenticated = false; // TODO: access only through getter
  isBrowser = isPlatformBrowser(this.platformId);
  userDetails: any;
  idToken: any;
  showDashboard = false;
  private readonly sub: any;
  private readonly menuSub: Subscription;
  private readonly routeSub: Subscription;
  secondaryNavigation: Array<CmsMenuTreeItem>;
  currentUrl: string;

  constructor(
    private readonly menuStore: Store<MenuState>,
    private readonly cd: ChangeDetectorRef,
    private readonly keycloakService: KeycloakService,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    private readonly checkRouteService: CheckRouteService,
    private readonly appStore: Store<State>,
    private readonly router: Router
  ) {
    super();
  }

  async ngOnInit(): Promise<void> {
    // TODO: import all routes from cms
    // https://dev.european-language-grid.eu/cms/all-pages?_format=hal_json

    this.router.events.subscribe((event: NavigationEnd | null) => {
      if (event instanceof NavigationEnd) {
        console.log('Navigation happened', event.url);
        this.currentUrl = event.url;
        if (document.activeElement instanceof HTMLElement) document.activeElement.blur();
        var clist = document.getElementsByTagName('input');
        for (var i = 0; i < clist.length; ++i) {
          clist[i].checked = false;
        }
      }
    });

    this.secondaryNavigation = undefined;

    this.title = 'APP_NAME';
    if (this.isBrowser) {
      this.appStore
        .select((st: State) => st.auth)
        .pipe(
          map(fields => (fields as unknown) as AuthState),
          distinctUntilChanged()
        )
        .subscribe(_state => {
          this.isAuthenticated = _state.isAuthenticated;
          if (this.isAuthenticated) {
            this.userDetails = _state.userProfile;
            this.idToken = _state.idToken;
            this.showDashboard = true;
            this.cd.markForCheck();
          }
        });
    }

    // this.menuStore.dispatch(new MenuAction.GetMenu('main'));

    combineLatest([
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).menu),
        distinctUntilChanged()
      ),
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).currentRoute),
        distinctUntilChanged()
      )
    ])
      .pipe(filter(([menuData, curRoute]) => menuData !== undefined))
      .subscribe(([menuData, curRoute]) => {
        this.siteMenuItems = ((menuData as unknown) as CmsMenuTree).menuItems; // .menuItems;
        if (curRoute !== '/') {
          this.secondaryNavigation = HeaderComponent.checkForSecondaryNavigation(this.siteMenuItems, curRoute);
        }

        this.importRoutes(this.siteMenuItems);

        this.externalMenuAvailable = true;

        this.cd.markForCheck();
      });
  }

  static checkForSecondaryNavigation(siteMenuItems: Array<CmsMenuTreeItem>, curRoute: string): Array<CmsMenuTreeItem> | undefined {
    const firstLevel = curRoute.split('/');

    let secNavigation: Array<CmsMenuTreeItem>;
    if (siteMenuItems) {
      siteMenuItems.forEach(menuItem => {
        menuItem.active = false;
        menuItem.childActive = false;
        menuItem.tabindex = false;
        if (menuItem.below) {
          menuItem.tabindex = true;
          menuItem.below.forEach(subMenuItem => {
            subMenuItem.active = false;
            subMenuItem.childActive = false;
            subMenuItem.tabindex = false;
            if (subMenuItem.below) {
              subMenuItem.tabindex = true;
              subMenuItem.below.forEach(thirdLevelMenu => {
                thirdLevelMenu.active = false;
              });
            }
          });
        }
      });
      siteMenuItems.forEach(menuItem => {
        if (menuItem.relative.replace('/cms/', '/') === `/${firstLevel[1]}` && menuItem.below) {
          secNavigation = menuItem.below;
        }
        if (menuItem.relative.replace('/cms/', '/') === curRoute) {
          menuItem.active = true;
        }

        if (menuItem.below) {
          menuItem.below.forEach(subMenuItem => {
            if (subMenuItem.relative.replace('/cms/', '/') === `/${firstLevel[1]}`) {
              secNavigation = menuItem.below;
            }
            if (subMenuItem.relative.replace('/cms/', '/').replace('news-page', 'news') === curRoute) {
              subMenuItem.active = true;
              menuItem.childActive = true;
            }

            // console.log("check", subMenuItem.options, (subMenuItem.options as CmsMenuOption).fragment, `/${firstLevel[1]}`);
            if (
              subMenuItem.options &&
              (subMenuItem.options as CmsMenuOption).fragment &&
              (subMenuItem.options as CmsMenuOption).fragment.replace('/cms/', '/') === `${firstLevel[1]}`
            ) {
              secNavigation = menuItem.below;
            }

            if (subMenuItem.below) {
              subMenuItem.below.forEach(thirdLevelMenu => {
                if (thirdLevelMenu.relative.replace('/cms/', '/') === `/${firstLevel[1]}`) {
                  secNavigation = menuItem.below;
                }
                if (thirdLevelMenu.relative.replace('/cms/', '/') === curRoute) {
                  thirdLevelMenu.active = true;
                  subMenuItem.childActive = true;
                  menuItem.childActive = true;
                }
              });
            }
          });
        }
      });
    }

    return secNavigation;
  }

  importRoutes(menuItems: Array<CmsMenuTreeItem>): void {
    if (menuItems && menuItems.length > 0) {
      for (const item of menuItems) {
        if (item.field_elg_menu_link_type === 'React') {
          this.checkRouteService.addRoute(`/catalogue${item.relative.replace('/cms/', '')}`);
        } else if (item.field_elg_menu_link_type === 'External') {
          // there is no need to add router path for external resource
        } else {
          this.checkRouteService.addRoute(item.relative.replace('/cms/', ''));
        }
      }
    }
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  };

  async logout(): Promise<void> {
    if (this.isBrowser) {
      return this.keycloakService.logout();
    }
  }

  async login(): Promise<void> {
    if (this.isBrowser) {
      return this.keycloakService.login();
    }
  }

  navigateToDashboard($event: Event): void {
    $event.preventDefault();

    if (this.isBrowser) {
      window.location.href = `/catalogue/mygrid/`;
    }
  }

  // eslint-disable-next-line class-methods-use-this
  prepareCMSRoute(rawUrl: string): Array<string> {
    // take out '/cms/'from url
    // then
    // split url into pieces without '/'
    const targetUrl = rawUrl.replace('/cms/', '').split('/');

    // return url in form ['/', 'firstPath', 'secondPath', ...]
    return ['/', ...targetUrl];
  }
}
