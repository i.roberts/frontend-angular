import { ShowdownComponent } from './showdown.component';

export const routes = [
  {
    path: '',
    component: ShowdownComponent,
    data: {
      meta: {
        title: 'Showdown tilte',
        description: 'Showdown desc'
      }
    }
  }
];
