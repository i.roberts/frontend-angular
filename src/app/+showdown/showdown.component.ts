import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BaseComponent } from '../framework/core';
import { routeAnimation } from '../shared';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface Section {
  name: string;
  updated: Date;
}

/**
 * Food data with nested structure.
 * Each node has a name and an optiona list of children.
 */
interface FoodNode {
  name: string;
  children?: Array<FoodNode>;
}

const TREE_DATA: Array<FoodNode> = [
  {
    name: 'Fruit',
    children: [
      {name: 'Apple'},
      {name: 'Banana'},
      {name: 'Fruit loops'},
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli'},
          {name: 'Brussel sprouts'},
        ]
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins'},
          {name: 'Carrots'},
        ]
      },
    ]
  },
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

export interface Fruit {
  name: string;
}

@Component({
  templateUrl: './showdown.component.html',
  styleUrls: ['showdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routeAnimation],
  encapsulation: ViewEncapsulation.None
})
export class ShowdownComponent extends BaseComponent implements OnInit{
    elemList: Array<string>;

    toggle = false;
 

  toggleVisibility(elem: string):void{

    const index = this.elemList.indexOf(elem, 0);
    if (index > -1) {
      this.elemList.splice(index, 1);
    }
    else {
    this.elemList.push(elem);
   }  

    this.toggle = !this.toggle
  }

  isElemInvisible(elem: string): boolean{ 
    const index = this.elemList.indexOf(elem, 0);
    if (index > -1) {
      return true;
     } else {
      return false;
     }
  }

  ngOnInit():void {

    this.elemList = [];
  }
  
  private readonly transformer = (node: FoodNode, level: number) =>
  ({
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    expandable: !!node.children && node.children.length > 0,
    name: node.name,
    level,
  })








}
