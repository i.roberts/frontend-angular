import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../framework/core';
import { MaterialModule } from '../framework/material';

import { ShowdownComponent } from './showdown.component';
import { routes } from './showdown.routes';


@NgModule({
  imports: [CommonModule, 
    FormsModule, 
    ReactiveFormsModule, 
    RouterModule.forChild(routes), 
    SharedModule, 
    MaterialModule],
  declarations: [ShowdownComponent],
  providers: []
})
export class ShowdownModule {}
