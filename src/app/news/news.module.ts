import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CmsMenuService } from '../+cms/cms-menu.service';
import { CmsService } from '../+cms/cms.service';
import { CommonModule } from '@angular/common';
import { DomService } from '../+cms/dom.service';
import { DynamicModule } from '../+cms/dynamiccontent/dynamic.module';
import { DynamicService } from '../+cms/dynamiccontent/dynamic.service';
import { MaterialModule } from '../framework/material';
import { NewsComponent } from './news.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './news.routes';
import { SearchModule } from '../search/search.module';
import { SharedModule } from '../framework/core';

@NgModule({
  imports: [
    CommonModule,

    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    MaterialModule,
    DynamicModule,
    SearchModule
  ],
  providers: [CmsService, CmsMenuService, DomService, DynamicService],
  declarations: [NewsComponent]
})
export class NewsModule {}

