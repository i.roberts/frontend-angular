/* eslint-disable class-methods-use-this */
import { isPlatformBrowser, Location, formatDate } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  AfterViewChecked,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewEncapsulation,
  Renderer2
} from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { distinctUntilChanged, map, filter } from 'rxjs/operators';
import { WebformElement, WebFormT, WebformError } from '../library/webforms/webforms.model';
import { routeAnimation } from '../shared';
import { MenuState } from '../store/menu/menu.state';
import { State } from '../store/state';
import { WebformState } from '../store/webforms/webform.state';
import { CmsState } from '../store/cms/cms.state';
import * as CmsAction from '../store/cms/cms.actions';

import { CmsSubmenuTree } from '../library/menu/menu.model';
import * as MenuAction from '../store/menu/menu.actions';
import * as WebformAction from '../store/webforms/webform.actions';
import { CmsPageContent, CmsService, Pager } from '../+cms/cms.service';
import { DynamicService } from '../+cms/dynamiccontent/dynamic.service';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { DynamicServerService } from '../+cms/dynamiccontent/dynamic-server.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [routeAnimation]
})
export class NewsComponent implements OnInit {
  isLoading: boolean;
  cmsPageContent: CmsPageContent;
  cmsNewsContent: Array<CmsPageContent> = [];
  isBrowser = isPlatformBrowser(this.platformId);
  noInlineFallback = ['tapsana']; // saraksts ar to statisko html lapu identifikatoriem,
  // kurām nav jāpielieto inlineFallback pipe
  sectionParsed: { [section: string]: boolean } = {
    'page-content-section': false
  };

  /**
   * Class list in content that comes from CMS that will be treated as placeholder and replaced with dynamic content
   */
  classesToParse: Array<string> = [
    'elg-link',
    'elg-catalogue-link',
    'elg-link-outlined',
    'elg-link-anchor',
    'elg-button-primary',
    'elg-button-accent',
    'elg-button-alternate',
    'topic-box',
    'topic-box-catalogue',
    'widget-top-categories',
    'widget-recently-updated',
    'widget-most-popular',
    'widget-latest-added',
    'widget-latest-added-resources',
    'widget-recently-updated-resources',
    'widget-latest-added-projects',
    'widget-latest-added-organizations',
    'become-provider-section',
    'become-provider-section-unregistered',
    'become-provider-section-applied'
  ];

  submenus: Array<CmsSubmenuTree>;
  hasSubmenus = false;
  currentSubmenu: CmsSubmenuTree;
  isSubmenu = false;
  parentLink = '';
  webformFields: WebFormT;
  webformElements: Array<WebformElement> = [];
  pageWebformId: string;
  pageWebform: FormGroup;
  pageHasWebform = false;
  pageWebformSubmitting = false;
  pageWebformError: any;
  pageWebformSuccess: any;
  pager: Pager;
  currentPageNumber: string;
  private pageLoading = true;
  private readonly newPageLoading = false;
  private sub: any;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly config: ConfigService,
    private readonly router: Router,
    private readonly elRef: ElementRef,
    private readonly cmsService: CmsService,
    private readonly dynamicService: DynamicService,
    private readonly dynamicServerService: DynamicServerService,
    private readonly cd: ChangeDetectorRef,
    private readonly menuStore: Store<MenuState>,
    private readonly wfStore: Store<State>,
    private readonly cmsStore: Store<State>,
    private readonly fb: FormBuilder,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly renderer: Renderer2,
    @Inject(PLATFORM_ID) private readonly platformId: any
  ) {
    this.cmsPageContent = new CmsPageContent();
  }

  ngOnInit(): void {
    this.sub = this.route.paramMap.subscribe(params => {
      this.currentPageNumber = params.get('pagenumber') === null ? '0' : params.get('pagenumber');
      this.currentPageNumber =
        parseInt(this.currentPageNumber, 10) > 0
          ? (((parseInt(this.currentPageNumber, 10) - 1) as unknown) as string)
          : this.currentPageNumber;

      this.cmsStore.dispatch(new CmsAction.GetNewsPage(this.currentPageNumber));

      // tslint:disable-next-line:forin
      for (const i in this.sectionParsed) {
        this.sectionParsed[i] = false;
      }

      this.cmsStore
        .select((st: State) => st.cms)
        .pipe(
          map(cmsState => ((cmsState as unknown) as CmsState).newsData),
          filter(cmsState => cmsState !== null),
          distinctUntilChanged()
        )
        .subscribe(cont => {
          this.pager = cont['pager'];
          this.cmsNewsContent = [];
          this.dynamicService.emptyHTMLContainer('.page-content-section');

          cont['rows'].forEach(element => {
            this.cmsPageContent = new CmsPageContent();
            this.cmsPageContent.loadString(
              element,
              this.config.getSettings('system.cms_base_url'),
              this.config.getSettings('system.cms_root_url'),
              this.config.getSettings('system.applicationUrl')
            );

            this.cmsNewsContent.push(this.cmsPageContent);
          });
          // this.cmsNewsContent
          // console.log('this.cmsNewsContent: ', this.cmsNewsContent);

          this.cmsPageContent.loading = false;
          this.isLoading = false;
          this.pageLoading = true;

          // tslint:disable-next-line:forin
          for (const i in this.sectionParsed) {
            this.sectionParsed[i] = false;
          }

          this.pageHasWebform = false;

          this.menuStore.dispatch(new MenuAction.SetCurrentRoute(this.router.url));
          this.cd.markForCheck();
        });
    });

    this.isBrowser = isPlatformBrowser(this.platformId);
  }
  getRoute(event: any): void {
    const goRoute = event.target.getAttribute('data-link');
    if (goRoute) {
      this.router
        .navigate([goRoute])
        .catch(err => {
          console.log(err);
        })
        .then(() => {
          console.log('');
        })
        .catch(() => 'obligatory catch');
    }
  }

  ngAfterViewChecked(): void {
    if (this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      let pageContentParsed = false;
      const newsContent: Array<HTMLElement> = [];
      this.cmsNewsContent.forEach(element => {
        const ret: boolean | HTMLElement = this.dynamicService.parseSection(
          'article-content-section',
          this.sectionParsed,
          element.body,
          this.elRef,
          '.article-content-section',
          this.classesToParse,
          true
        );
        if (ret instanceof HTMLElement) {
          newsContent.push(ret);
        }

        pageContentParsed = true;
      });

      newsContent.forEach((news, index) => {
        const div = this.dynamicService.createHTMLElement('', 'div', `article_div article_div_${index}`, `article_div_${index}`);
        const span_div = this.dynamicService.createHTMLElement(
          '',
          'div',
          `article_span_div article_span_div_${index}`,
          `article_span_div_${index}`
        );
        const author = this.dynamicService.createHTMLElement(
          `by ${this.cmsNewsContent[index].author}`,
          'span',
          'article_author',
          `article_author_${index}`
        );
        const publish_date = this.dynamicService.createHTMLElement(
          `Posted on ${formatDate(this.cmsNewsContent[index].created, 'dd/MM/yyyy', 'en-US')}`,
          'span',
          'article_published',
          `article_published_${index}`
        );
        const title = this.dynamicService.createHTMLElement(
          this.cmsNewsContent[index].title,
          'h1',
          'article_title',
          `article_title_${index}`
        );
        const link = this.dynamicService.createHTMLElement(
          '',
          'a',
          `article_link_${index}`,
          `article_link_${index}`,
          this.cmsNewsContent[index].view_node
        );
        this.dynamicService.appendHtml('.page-content-section', link, this.elRef, true);
        this.dynamicService.appendHtml(`.article_link_${index}`, div, this.elRef, true);
        this.dynamicService.appendHtml(`.article_div_${index}`, span_div, this.elRef, true);
        this.dynamicService.appendHtml(`.article_span_div_${index}`, publish_date, this.elRef, true);
        this.dynamicService.appendHtml(`.article_span_div_${index}`, author, this.elRef, true);
        this.dynamicService.appendHtml(`.article_div_${index}`, title, this.elRef, true);
        this.dynamicService.appendHtml(`.article_div_${index}`, news, this.elRef, true);
      });

      if (pageContentParsed) {
        this.sectionParsed['page-content-section'] = true;
        this.pageLoading = false;
      } else {
        this.pageLoading = true;
      }

      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
      this.cd.markForCheck();
    } else if (!this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      const pageContentParsed = this.dynamicServerService.parseSection(
        'page-content-section',
        this.sectionParsed,
        this.cmsPageContent.body,
        this.elRef,
        '.page-content-section',
        this.classesToParse
      );
      const targetSection = this.renderer.selectRootElement('#page-content-section', true);
      const d2 = this.renderer.createElement('div');
      this.renderer.setProperty(d2, 'innerHTML', pageContentParsed.html());
      this.renderer.appendChild(targetSection, d2);

      if (pageContentParsed !== false) {
        this.sectionParsed['page-content-section'] = true;
      }
      this.pageLoading = false;
      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
    }
  }

  shouldIterate(elem: any): boolean {
    if (Object.prototype.toString.call(elem) === '[object Object]') {
      return true;
    } else {
      return false;
    }
  }

  counter(i: number): Array<number> {
    return new Array(i);
  }

  addFormElement(elem: WebformElement): void {
    this.webformElements.push(elem);
  }

  getElemType(i: number): string {
    return this.webformElements[i]['#type'];
  }

  fromEntries<T>(entries: Array<[keyof T, T[keyof T]]>): T {
    return entries.reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {} as T);
  }

  submitForm(event): void {
    event.preventDefault();
    // eslint-disable-next-line @typescript-eslint/unbound-method
    const formObj = [];
    formObj.push(['webform_id', this.pageWebformId]);
    (this.pageWebform.get('webformElems') as FormArray).controls.forEach((control: FormControl, i: number) => {
      if (this.webformElements[i]['#type'] !== 'webform_actions') {
        formObj.push([this.webformElements[i]['#name'], control.value]);
      }
    });

    this.pageWebformSubmitting = true;
    this.wfStore.dispatch(new WebformAction.PostWebform(this.fromEntries(formObj)));
  }

  resetForm(): void {
    this.pageWebform.reset();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

