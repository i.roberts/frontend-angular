import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { MaterialModule } from '../framework/material';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './silo.routes';
import { SearchModule } from '../search/search.module';
import { SharedModule } from '../framework/core';
import { SiloComponent } from './silo.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes), SharedModule, MaterialModule, SearchModule],
  declarations: [SiloComponent]
})
export class SiloModule {}

