import { ChangeDetectorRef,ChangeDetectionStrategy, Component, OnDestroy, Inject, Injector, OnInit, PLATFORM_ID, Output, EventEmitter  } from '@angular/core';
import { Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { BaseComponent } from '../framework/core';
import { languageActions, State } from '../store';

import { isPlatformBrowser } from "@angular/common";
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import * as MenuAction from '../store/menu/menu.actions';
import { CmsMenuTree, CmsMenuTreeItem } from '../library/menu/menu.model';
import { MenuState } from '../store/menu/menu.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.ssr.html',
  styleUrls: ['./layout-ssr/main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponentSsr extends BaseComponent implements OnInit {
  isAuthenticated = false; // TODO: access only through getter
  isBrowser = isPlatformBrowser(this.platformId);
  userDetails: any;
  siteMenuItems: Array<CmsMenuTreeItem>;
  feedbackItems: Array<CmsMenuTreeItem>;
  externalMenuAvailable: boolean;

  constructor(
    private readonly store$: Store<State>,
    private readonly config: ConfigService,
    private readonly menuStore: Store<MenuState>,
    private readonly cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private readonly platformId: any,
  ) {
    super();
  }

  ngOnInit(): void {

    const settings = this.config.getSettings('i18n');
    this.store$.dispatch(languageActions.i18nInitLanguage(settings));
    if (this.isBrowser) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
    }

    this.menuStore.dispatch(new MenuAction.GetMenu('main'));

    combineLatest([
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).menu ),
        distinctUntilChanged()
      ),
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).currentRoute ),
        distinctUntilChanged()
      )
    ]
    ).subscribe(([menuData, curRoute]) => {
      this.siteMenuItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;
      // this.drawSubmenu(this.siteMenuItems, curRoute);

      this.externalMenuAvailable = true;

      this.cd.markForCheck();
    });


    this.menuStore.dispatch(new MenuAction.GetFeedbackMenu('feedback'));

    this.menuStore
      .select('menu')
      .pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).feedbackMenu),
        distinctUntilChanged()
      )
      .subscribe(menuData => {
        this.feedbackItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;

        this.cd.markForCheck();
      });

  }

  async logout(): Promise<void> {
    if (this.isBrowser) {

    }
  }

  async login(): Promise<void> {
    if (this.isBrowser) {

    }
  }
}
