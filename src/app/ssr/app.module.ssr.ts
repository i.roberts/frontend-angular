import { BrowserModule, makeStateKey } from '@angular/platform-browser';
import { configFactory, CoreModule, metaFactory, SharedModule } from '../framework/core';
import { ConfigLoader, ConfigService } from '@ngx-config/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { I18NModule, translateFactory } from '../framework/i18n';
import { Injector, NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { TranslateLoader, TranslateService } from '@ngx-translate/core';

import { AnalyticsModule } from '../framework/analytics';
import { ANGULARTICS2_TOKEN } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { AppComponentSsr } from './app.component.ssr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CmsFeatureCheckRouteModule } from '../check-route/cms-feature-check-route.module';
import { CmsModule } from '../+cms/cms.module';
import { DomService } from '../+cms/dom.service';
import { DynamicModule } from '../+cms/dynamiccontent/dynamic.module';
import { FootComponent } from './layout-ssr/foot/foot.component';
import { HeaderComponent } from './layout-ssr/header.component';
import { MaterialModule } from '../framework/material';
import { MetaLoader } from '@ngx-meta/core';
import { routes } from './app.routes';
import { SearchModule } from '../search/search.module';
import { StoreModule } from '../store';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { UniversalRelativeInterceptor } from './ssr.interceptor';

@NgModule({
  declarations: [FootComponent, HeaderComponent, AppComponentSsr],
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app-id' }),
    BrowserAnimationsModule,
    HttpClientModule,
    TransferHttpCacheModule,
    CmsFeatureCheckRouteModule,
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      preloadingStrategy: PreloadAllModules
    }),
    AnalyticsModule.forRoot([
      {
        provide: ANGULARTICS2_TOKEN,
        useValue: {
          providers: [Angulartics2GoogleAnalytics],
          settings: {}
        }
      }
    ]),
    CoreModule.forRoot([
      {
        provide: ConfigLoader,
        useFactory: configFactory,
        deps: [Injector]
      },
      {
        provide: MetaLoader,
        useFactory: metaFactory,
        deps: [ConfigService, TranslateService]
      }
    ]),
    SharedModule,
    I18NModule.forRoot([
      {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [HttpClient]
      }
    ]),
    MaterialModule,
    StoreModule.forRoot(),
    DynamicModule,
    FormsModule,
    ReactiveFormsModule,
    SearchModule,
    CmsModule
  ],
  providers: [
    DomService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UniversalRelativeInterceptor,
      multi: true
    }
  ]
})
export class AppModuleSsr {}

