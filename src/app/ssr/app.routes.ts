import { ChangeLanguageComponent } from '../framework/i18n';
import { CheckRouteComponent } from '../check-route/check-route.component';
import { CmsComponent } from '../+cms/cms.component';
import { AppComponentSsr } from './app.component.ssr';

export const routes = [
  {
    path: '',
    component: CmsComponent
  },
  {
    path: 'news',
    loadChildren: async () => import('../news/news.module').then(m => m.NewsModule )
  },
  {
    path: 'check-route', component: CheckRouteComponent
  },
  {
    path: 'change-language/:languageCode',
    component: ChangeLanguageComponent
  },
  {
    path: '**',
    redirectTo: 'check-route',
    pathMatch: 'full'
  }
];
