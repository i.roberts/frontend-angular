import {
  ApplicationRef,
  ComponentFactoryResolver,
  EmbeddedViewRef,
  Injectable,
  Injector
} from '@angular/core';

import { DynamicComponent } from './dynamiccontent/dynamic.component';
import { DynamicServerComponent } from './dynamiccontent/dynamic-server.component';
import { InteractiveData } from './dynamiccontent/interactive.model';

@Injectable()
export class DomService {

constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly appRef: ApplicationRef,
    private readonly injector: Injector
) { }

  appendInteractiveElement(component: any, type: string, interactiveData: InteractiveData, platform = 'browser'):HTMLElement {


    if (platform === 'browser') {
      const componentRef = this.componentFactoryResolver
      .resolveComponentFactory(component)
      .create(this.injector);



    this.appRef.attachView(componentRef.hostView);
      (componentRef.instance as DynamicComponent).elemData = interactiveData;
      componentRef.changeDetectorRef.detectChanges();

      return (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    } else {
      const componentRef = this.componentFactoryResolver
      .resolveComponentFactory(component)
      .create(this.injector);



    this.appRef.attachView(componentRef.hostView);
      (componentRef.instance as DynamicServerComponent).elemData = interactiveData;
      componentRef.changeDetectorRef.detectChanges();

      return (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    }



  }
}
