/* eslint-disable class-methods-use-this */
import { Component, Input, OnInit, ViewEncapsulation, ChangeDetectorRef, PLATFORM_ID, Inject } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle } from '@angular/platform-browser';

import { MaterialElevationDirective } from '../../shared/directives/elevate.directive';

import { InteractiveData } from './interactive.model';

import { Store } from '@ngrx/store';
import { State } from '../../store/state';

import {
  TopCategories,
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResultsCollection,
  FeaturedToolsAndService
} from '../../library/search/models/search-results.model';
import * as SearchAction from '../../store/search/search.actions';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { ProviderApplicantService } from '../../request-provider/provider-applicant.service';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AuthState } from '../../store/auth/auth.state';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DynamicComponent implements OnInit {
  // @Input() type:any;
  // @Input() entryId:any;
  // @Input() imageSource:any;
  // @Input() textData:any;
  topCategories: TopCategories;
  toolsAndServicesFeat: FeaturedToolsAndService;
  recentlyUpdatedTech: RecentlyUpdatedTech;
  mostPopularTech: MostPopularTech;
  recentlyUpdated: RecentlyUpdatedTech;
  mostPopular: MostPopularTech;
  latestAddedTech: SearchResultsCollection;
  latestAddedRes: SearchResultsCollection;
  recentlyUpdatedRes: SearchResultsCollection;
  latestAddedProjects: SearchResultsCollection;
  latestAddedOrganizations: SearchResultsCollection;
  isFirstButtonActived = true;
  isSecondButtonActived = false;
  isThirdButtonActived = false;
  showAllCategories = false;
  showAllRecents = false;
  showAllPopulars = false;
  showAllLatestAdded = false;
  showAllLatestAddedRes = false;
  showAllLatestAddedProjects = false;
  showAllLatestAddedOrganizations = false;
  showAllResRecents = false;
  @Input() elemData: InteractiveData;
  cssClasses = '';
  isExternal = false;
  isBrowser = isPlatformBrowser(this.platformId);
  showBecomeProviderBanner = false;
  showRegisterAndBecomeProviderBanner = false;
  showAppliedForProviderBanner = false;
  idToken: any;
  isAuthenticated: boolean = false;

  entry = '';
  entryDate = '';
  imageLink = '';
  entryCount = 0;
  multimediaCount = 0;

  sanitizedStyle: SafeStyle = '';
  sanitizedHtml: SafeHtml = '';
  currentTSWidgetTab: number = 0;
  currentLRWidgetTab: number = 0;
  currentOWidgetTab: number = 0;
  currentTSPage: number;
  currentTSPageMP: number;
  totalTSPages: number;
  totalTSPagesMP: number;
  currentLRPage: any;
  currentLRPageMP: any;
  totalLRPage: number;
  totalLRPageMP: number;
  currentOPage: any;
  currentOPageMP: any;
  totalOPage: number;
  totalOPageMP: number;
  currentTSPageFeat: any;
  totalTSPagesFeat: number;
  currentLRPageFeat: any;
  totalLRPagesFeat: number;
  totalLRPageFav: number;
  currentOPageFeat: any;
  totalOPageFeat: number;

  constructor(
    private readonly sanitization: DomSanitizer,
    private readonly appStore: Store<State>,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    private readonly providerApplicant: ProviderApplicantService
  ) {}

  // Pārbauda, vai skaitli paskaidrojošam vārdam jābūt daudzskaitlī
  static plural(val: number): boolean {
    let _val = val;
    if (_val === undefined) {
      _val = 0;
    }
    if (_val % 10 === 1 && _val % 100 !== 11) {
      return false;
    }

    return true;
  }

  getServiceUrl(entity_type, resource_type, id): string {
    let url = '';
    switch (entity_type) {
      case 'Project':
        url = `/catalogue/project/${id}`;
        break;
      case 'Organization':
        url = `/catalogue/organization/${id}`;
        break;
      default:
        switch (resource_type) {
          case 'Tool/Service':
            url = `/catalogue/tool-service/${id}`;
            break;
          case 'Corpus':
            url = `/catalogue/corpus/${id}`;
            break;
          case 'Lexical/Conceptual resource':
            url = `/catalogue/lcr/${id}`;
            break;
          default:
            break;
        }
    }
    return url;
  }

  validURL(str: string): boolean {
    const pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i'
    ); // fragment locator

    return !!pattern.test(str);
  }

  ngOnInit(): void {
    this.appStore
      .select((appState: any) => appState.catalogue.currentTSWidgetTab)
      .subscribe(curTab => {
        this.currentTSWidgetTab = curTab;
      });

    this.appStore
      .select((appState: any) => appState.catalogue.currentLRWidgetTab)
      .subscribe(curTab => {
        this.currentLRWidgetTab = curTab;
      });

    this.appStore
      .select((appState: any) => appState.catalogue.currentOWidgetTab)
      .subscribe(curTab => {
        this.currentOWidgetTab = curTab;
      });

    this.appStore
      .select((st: State) => st.auth)
      .pipe(
        map(fields => (fields as unknown) as AuthState),
        distinctUntilChanged()
      )
      .subscribe(_state => {
        this.isAuthenticated = _state.isAuthenticated;

        if (this.isAuthenticated && isPlatformBrowser(this.platformId)) {
          this.showRegisterAndBecomeProviderBanner = false;

          this.idToken = _state.idToken;

          if (this.idToken.roles && this.isBrowser) {
            // eslint-disable-next-line @typescript-eslint/tslint/config
            if (
              this.providerApplicant.userIsProvider(this.idToken.roles) ||
              this.providerApplicant.userHasAppliedForProvider(this.idToken.sub)
            ) {
              this.showBecomeProviderBanner = false;
            } else {
              this.showBecomeProviderBanner = true;
            }

            // eslint-disable-next-line @typescript-eslint/tslint/config
            if (
              !this.providerApplicant.userIsProvider(this.idToken.roles) &&
              this.providerApplicant.userHasAppliedForProvider(this.idToken.sub)
            ) {
              this.showAppliedForProviderBanner = true;
            } else {
              this.showAppliedForProviderBanner = false;
            }
          } else if (this.providerApplicant.userHasAppliedForProvider(this.idToken.sub) && this.isBrowser) {
            this.showAppliedForProviderBanner = true;
          } else {
            this.showBecomeProviderBanner = true;
          }
        } else {
          this.showRegisterAndBecomeProviderBanner = true;
        }
      });

    if (this.elemData.link !== undefined) {
      this.entry = this.elemData.link;
    }
    this.isExternal = this.validURL(this.elemData.link);

    switch (this.elemData.type) {
      case 'elg-link':
        this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        break;
      case 'photo-modal':
        this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        break;
      case 'elg-catalogue-link':
        this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        break;
      case 'topic-box':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'topic-box-catalogue':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section-unregistered':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section-applied':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'widget-paging-ts-back':
        this.appStore
          .select((appState: any) => appState.catalogue.currentTSPageFeat)
          .subscribe(currentTSPageFeat => {
            this.currentTSPageFeat = currentTSPageFeat;
          });

        this.appStore
          .select((appState: any) => appState.catalogue.currentTSPage)
          .subscribe(currentTSPage => {
            this.currentTSPage = currentTSPage;
          });

        this.appStore
          .select((appState: any) => appState.catalogue.currentTSPageMP)
          .subscribe(currentTSPageMP => {
            this.currentTSPageMP = currentTSPageMP;
          });
        break;
      case 'widget-paging-ts-next':
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredToolsAndService),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPageFeat)
        ])
          .pipe(map(([featuredToolsAndService, currentTSPageFeat]) => ({ featuredToolsAndService, currentTSPageFeat })))
          .subscribe((latestData: any) => {
            this.currentTSPageFeat = latestData.currentTSPageFeat;
            this.totalTSPagesFeat = Math.ceil(latestData.featuredToolsAndService.results.length / 5 - 1);
          });

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.recentlyUpdatedTech),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPage)
        ])
          .pipe(map(([recentlyUpdatedTech, currentTSPage]) => ({ recentlyUpdatedTech, currentTSPage })))
          .subscribe((latestData: any) => {
            this.currentTSPage = latestData.currentTSPage;
            this.totalTSPages = latestData.recentlyUpdatedTech.results.length / 5 - 1;
          });

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularTech),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPageMP)
        ])
          .pipe(map(([mostPopularTech, currentTSPageMP]) => ({ mostPopularTech, currentTSPageMP })))
          .subscribe((latestData: any) => {
            this.currentTSPageMP = latestData.currentTSPageMP;
            this.totalTSPagesMP = latestData.mostPopularTech.results.length / 5 - 1;
          });

        break;
      case 'widget-services':
        this.appStore.dispatch(new SearchAction.GetFeaturedToolsAndService());
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredToolsAndService),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPageFeat)
        ])
          .pipe(map(([featuredToolsAndService, currentTSPageFeat]) => ({ featuredToolsAndService, currentTSPageFeat })))
          .subscribe((latestData: any) => {
            if (
              latestData.featuredToolsAndService &&
              latestData.featuredToolsAndService.results &&
              latestData.featuredToolsAndService.results.length !== undefined
            ) {
              this.toolsAndServicesFeat = latestData.featuredToolsAndService.results.slice(
                latestData.currentTSPageFeat * 5,
                latestData.currentTSPageFeat * 5 + 5
              );
            }
          });

        this.appStore.dispatch(new SearchAction.GetRecentlyUpdated());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.recentlyUpdatedTech),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPage)
        ])
          .pipe(map(([recentlyUpdatedTech, currentTSPage]) => ({ recentlyUpdatedTech, currentTSPage })))
          .subscribe((latestData: any) => {
            if (
              latestData.recentlyUpdatedTech &&
              latestData.recentlyUpdatedTech.results &&
              latestData.recentlyUpdatedTech.results.length !== undefined
            ) {
              this.recentlyUpdated = latestData.recentlyUpdatedTech.results.slice(
                latestData.currentTSPage * 5,
                latestData.currentTSPage * 5 + 5
              );
            }
          });

        this.appStore.dispatch(new SearchAction.GetMostPopular());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularTech),
          this.appStore.select((appState: any) => appState.catalogue.currentTSPageMP)
        ])
          .pipe(map(([mostPopularTech, currentTSPageMP]) => ({ mostPopularTech, currentTSPageMP })))
          .subscribe((latestData: any) => {
            if (
              latestData.mostPopularTech &&
              latestData.mostPopularTech.results &&
              latestData.mostPopularTech.results.length !== undefined
            ) {
              this.mostPopular = latestData.mostPopularTech.results.slice(
                latestData.currentTSPageMP * 5,
                latestData.currentTSPageMP * 5 + 5
              );
            }
          });
        break;
      case 'widget-paging-lr-back':
        this.appStore
          .select((appState: any) => appState.catalogue.currentLRPageFeat)
          .subscribe(currentLRPageFeat => {
            this.currentLRPageFeat = currentLRPageFeat;
          });
        this.appStore
          .select((appState: any) => appState.catalogue.currentLRPage)
          .subscribe(currentLRPage => {
            this.currentLRPage = currentLRPage;
          });
        this.appStore
          .select((appState: any) => appState.catalogue.currentLRPageMP)
          .subscribe(currentLRPageMP => {
            this.currentLRPageMP = currentLRPageMP;
          });
        break;
      case 'widget-paging-lr-next':
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredLanguageResource),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPageFeat)
        ])
          .pipe(map(([featuredLanguageResource, currentLRPageFeat]) => ({ featuredLanguageResource, currentLRPageFeat })))
          .subscribe((latestData: any) => {
            this.currentLRPageFeat = latestData.currentLRPageFeat;
            this.totalLRPageFav = latestData.featuredLanguageResource.results.length / 5 - 1;
          });

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.recentlyUpdatedRes),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPage)
        ])
          .pipe(map(([recentlyUpdatedRes, currentLRPage]) => ({ recentlyUpdatedRes, currentLRPage })))
          .subscribe((latestData: any) => {
            this.currentLRPage = latestData.currentLRPage;
            this.totalLRPage = latestData.recentlyUpdatedRes.results.length / 5 - 1;
          });

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularTech),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPageMP)
        ])
          .pipe(map(([mostPopularTech, currentLRPageMP]) => ({ mostPopularTech, currentLRPageMP })))
          .subscribe((latestData: any) => {
            this.currentLRPageMP = latestData.currentLRPageMP;
            this.totalLRPageMP = latestData.mostPopularTech.results.length / 5 - 1;
          });
        break;
      case 'widget-lr':
        this.appStore.dispatch(new SearchAction.GetFeaturedLanguageResources());
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredLanguageResource),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPageFeat)
        ])
          .pipe(map(([featuredLanguageResource, currentLRPageFeat]) => ({ featuredLanguageResource, currentLRPageFeat })))
          .subscribe((latestData: any) => {
            if (
              latestData.featuredLanguageResource &&
              latestData.featuredLanguageResource.results &&
              latestData.featuredLanguageResource.results.length !== undefined
            ) {
              this.toolsAndServicesFeat = latestData.featuredLanguageResource.results.slice(
                latestData.currentLRPageFeat * 5,
                latestData.currentLRPageFeat * 5 + 5
              );
            }
          });

        this.appStore.dispatch(new SearchAction.GetRecentlyUpdatedResource());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.recentlyUpdatedRes),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPage)
        ])
          .pipe(map(([recentlyUpdatedRes, currentLRPage]) => ({ recentlyUpdatedRes, currentLRPage })))
          .subscribe((latestData: any) => {
            if (
              latestData.recentlyUpdatedRes &&
              latestData.recentlyUpdatedRes.results &&
              latestData.recentlyUpdatedRes.results.length !== undefined
            ) {
              this.recentlyUpdated = latestData.recentlyUpdatedRes.results.slice(
                latestData.currentLRPage * 5,
                latestData.currentLRPage * 5 + 5
              );
              // this.cd.markForCheck();
            }
          });

        this.appStore.dispatch(new SearchAction.GetMostPopularResource());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularResource),
          this.appStore.select((appState: any) => appState.catalogue.currentLRPageMP)
        ])
          .pipe(map(([mostPopularResource, currentLRPageMP]) => ({ mostPopularResource, currentLRPageMP })))
          .subscribe((latestData: any) => {
            if (
              latestData.mostPopularResource &&
              latestData.mostPopularResource.results &&
              latestData.mostPopularResource.results.length !== undefined
            ) {
              this.mostPopular = latestData.mostPopularResource.results.slice(
                latestData.currentLRPageMP * 5,
                latestData.currentLRPageMP * 5 + 5
              );
            }
          });
        break;
      case 'widget-paging-o-back':
        this.appStore
          .select((appState: any) => appState.catalogue.currentOPageFeat)
          .subscribe(currentOPageFeat => {
            this.currentOPageFeat = currentOPageFeat;
          });
        this.appStore
          .select((appState: any) => appState.catalogue.currentOPage)
          .subscribe(currentOPage => {
            this.currentOPage = currentOPage;
          });
        this.appStore
          .select((appState: any) => appState.catalogue.currentOPageMP)
          .subscribe(currentOPageMP => {
            this.currentOPageMP = currentOPageMP;
          });
        break;
      case 'widget-paging-o-next':
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredOrganizations),
          this.appStore.select((appState: any) => appState.catalogue.currentOPageFeat)
        ])
          .pipe(map(([featuredOrganizations, currentOPageFeat]) => ({ featuredOrganizations, currentOPageFeat })))
          .subscribe((latestData: any) => {
            this.currentOPageFeat = latestData.currentOPageFeat;
            this.totalOPageFeat = latestData.featuredOrganizations.results.length / 5 - 1;
          });
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.latestAddedOrganizations),
          this.appStore.select((appState: any) => appState.catalogue.currentOPage)
        ])
          .pipe(map(([latestAddedOrganizations, currentOPage]) => ({ latestAddedOrganizations, currentOPage })))
          .subscribe((latestData: any) => {
            this.currentOPage = latestData.currentOPage;
            this.totalOPage = latestData.latestAddedOrganizations.results.length / 5 - 1;
          });
        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularTech),
          this.appStore.select((appState: any) => appState.catalogue.currentOPageMP)
        ])
          .pipe(map(([mostPopularTech, currentOPageMP]) => ({ mostPopularTech, currentOPageMP })))
          .subscribe((latestData: any) => {
            this.currentOPageMP = latestData.currentOPageMP;
            this.totalOPageMP = latestData.mostPopularTech.results.length / 5 - 1;
          });
        break;
      case 'widget-organizations':
        this.appStore.dispatch(new SearchAction.GetFeaturedOrganizations());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.featuredOrganizations),
          this.appStore.select((appState: any) => appState.catalogue.currentOPageFeat)
        ])
          .pipe(map(([featuredOrganizations, currentOPageFeat]) => ({ featuredOrganizations, currentOPageFeat })))
          .subscribe((latestData: any) => {
            if (
              latestData.featuredOrganizations &&
              latestData.featuredOrganizations.results &&
              latestData.featuredOrganizations.results.length !== undefined
            ) {
              this.toolsAndServicesFeat = latestData.featuredOrganizations.results.slice(
                latestData.currentOPageFeat * 5,
                latestData.currentOPageFeat * 5 + 5
              );
              // this.cd.markForCheck();
            }
          });

        this.appStore.dispatch(new SearchAction.GetLastAddedOrganizations());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.latestAddedOrganizations),
          this.appStore.select((appState: any) => appState.catalogue.currentOPage)
        ])
          .pipe(map(([latestAddedOrganizations, currentOPage]) => ({ latestAddedOrganizations, currentOPage })))
          .subscribe((latestData: any) => {
            if (
              latestData.latestAddedOrganizations &&
              latestData.latestAddedOrganizations.results &&
              latestData.latestAddedOrganizations.results.length !== undefined
            ) {
              this.recentlyUpdated = latestData.latestAddedOrganizations.results.slice(
                latestData.currentOPage * 5,
                latestData.currentOPage * 5 + 5
              );
              // this.cd.markForCheck();
            }
          });

        this.appStore.dispatch(new SearchAction.GetMostPopularOrganizations());

        combineLatest([
          this.appStore.select((appState: any) => appState.catalogue.mostPopularOrganization),
          this.appStore.select((appState: any) => appState.catalogue.currentOPageMP)
        ])
          .pipe(map(([mostPopularOrganization, currentOPageMP]) => ({ mostPopularOrganization, currentOPageMP })))
          .subscribe((latestData: any) => {
            if (
              latestData.mostPopularOrganization &&
              latestData.mostPopularOrganization.results &&
              latestData.mostPopularOrganization.results.length !== undefined
            ) {
              this.mostPopular = latestData.mostPopularOrganization.results.slice(
                latestData.currentOPageMP * 5,
                latestData.currentOPageMP * 5 + 5
              );
            }
          });
        break;

      default:
        break;
    }
  }

  checkFirstButtonActived(widget: string): void {
    this.appStore.dispatch(new SearchAction.SetWidgetTab(widget, 0));

    this.isFirstButtonActived = true;
    this.isSecondButtonActived = false;
    this.isThirdButtonActived = false;
  }
  checkSecondButtonActived(widget: string): void {
    this.appStore.dispatch(new SearchAction.SetWidgetTab(widget, 1));

    this.isFirstButtonActived = false;
    this.isSecondButtonActived = true;
    this.isThirdButtonActived = false;
  }
  checkThirdButtonActived(widget: string): void {
    this.appStore.dispatch(new SearchAction.SetWidgetTab(widget, 2));

    this.isFirstButtonActived = false;
    this.isSecondButtonActived = false;
    this.isThirdButtonActived = true;
  }

  toggleShowAllCategories($event): void {
    $event.preventDefault();
    this.showAllCategories = !this.showAllCategories;
  }

  toggleShowAllPopular($event): void {
    $event.preventDefault();
    this.showAllPopulars = !this.showAllPopulars;
  }

  toggleShowAllRecent($event): void {
    $event.preventDefault();
    this.showAllRecents = !this.showAllRecents;
  }

  toggleShowAllResRecent($event): void {
    $event.preventDefault();
    this.showAllResRecents = !this.showAllResRecents;
  }

  toggleShowAllLatestAdded($event): void {
    $event.preventDefault();
    this.showAllLatestAdded = !this.showAllLatestAdded;
  }

  toggleShowAllLatestAddedRes($event): void {
    $event.preventDefault();
    this.showAllLatestAddedRes = !this.showAllLatestAddedRes;
  }

  toggleShowAllLatestAddedProjects($event): void {
    $event.preventDefault();
    this.showAllLatestAddedProjects = !this.showAllLatestAddedProjects;
  }

  toggleShowAllLatestAddedOrganizations($event): void {
    $event.preventDefault();
    this.showAllLatestAddedOrganizations = !this.showAllLatestAddedOrganizations;
  }

  navigate($event, url: string): void {
    $event.preventDefault();
    if (this.isBrowser) {
      window.location.href = url;
    }
  }

  widgetPrevPressed(trigerButton: string): void {
    if (trigerButton === 'tsBack' && this.currentTSWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesFavPrevPage());
    } else if (trigerButton === 'tsBack' && this.currentTSWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesMPPrevPage());
    } else if (trigerButton === 'tsBack' && this.currentTSWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesPrevPage());
    }

    if (trigerButton === 'lrBack' && this.currentLRWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesFavPrevPage());
    } else if (trigerButton === 'lrBack' && this.currentLRWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesMPPrevPage());
    } else if (trigerButton === 'lrBack' && this.currentLRWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesPrevPage());
    }

    if (trigerButton === 'oBack' && this.currentOWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsFavPrevPage());
    } else if (trigerButton === 'oBack' && this.currentOWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsMPPrevPage());
    } else if (trigerButton === 'oBack' && this.currentOWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsPrevPage());
    }
  }

  widgetNextPressed(trigerButton: string): void {
    if (trigerButton === 'tsNext' && this.currentTSWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesFavNextPage());
    } else if (trigerButton === 'tsNext' && this.currentTSWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesMPNextPage());
    } else if (trigerButton === 'tsNext' && this.currentTSWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetToolsServicesNextPage());
    }

    if (trigerButton === 'lrNext' && this.currentLRWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesFavNextPage());
    } else if (trigerButton === 'lrNext' && this.currentLRWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesMPNextPage());
    } else if (trigerButton === 'lrNext' && this.currentLRWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetLanguageResourcesNextPage());
    }

    if (trigerButton === 'oNext' && this.currentOWidgetTab === 0) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsFavNextPage());
    } else if (trigerButton === 'oNext' && this.currentOWidgetTab === 1) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsMPNextPage());
    } else if (trigerButton === 'oNext' && this.currentOWidgetTab === 2) {
      this.appStore.dispatch(new SearchAction.SetOrganizationsNextPage());
    }
  }

  navigateService(resource_type, id): void {
    let url = '';
    switch (resource_type) {
      case 'Tool/Service':
        url = `/catalogue/resource/service/tool/${id}`;
        break;
      case 'Corpus':
        url = `/catalogue/resource/service/corpus/${id}`;
        break;
      default:
        break;
    }
    // $event.preventDefault();
    if (this.isBrowser) {
      window.location.href = url;
    }
  }

  scrollToAnchor(anchor: string): void {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    const el = document.getElementById(anchor);
    el.scrollIntoView({ behavior: 'smooth' });
  }

  hasClass(classList: DOMTokenList | string | null, classNeedle: string): boolean {
    if (classList === undefined) {
      return false;
    }

    if (typeof classList === 'string') {
      return classList.includes(classNeedle);
    }

    return this.elemData.classList.contains(classNeedle);
  }

  encodeKey(key: string): string {
    const temKey = key;

    return encodeURIComponent(temKey);
  }

  openModal(id: string) {
    document.getElementById(id).style.visibility = 'visible';
    document.getElementById(id).style.opacity = '1';
    document.getElementById(id).style.pointerEvents = 'auto';
    document.getElementById('img-' + id).setAttribute('src', document.getElementById('img-' + id).getAttribute('data-src'));
  }
  closeModal(id: string) {
    document.getElementById(id).style.visibility = 'hidden';
    document.getElementById(id).style.opacity = '0';
    document.getElementById(id).style.pointerEvents = 'none';
  }
}
