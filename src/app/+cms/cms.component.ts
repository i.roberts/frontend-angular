import { isPlatformBrowser } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DoCheck,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
  Renderer2
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { BaseComponent } from '../framework/core';
import { routeAnimation } from '../shared';
import { State } from '../store/state';

import * as MenuAction from '../store/menu/menu.actions';
import { MenuState } from '../store/menu/menu.state';
import { CmsState, CatalogueStats } from '../store/cms/cms.state';

import * as CmsAction from '../store/cms/cms.actions';
import { CmsNewsContent, CmsPageContent, CmsService } from './cms.service';
import { DynamicService } from './dynamiccontent/dynamic.service';
import { DynamicServerService } from './dynamiccontent/dynamic-server.service';

import { Meta, Title } from '@angular/platform-browser';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

const intToMonth = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

@Component({
  templateUrl: './cms.component.html',
  styleUrls: ['cms.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [routeAnimation]
})
export class CmsComponent extends BaseComponent implements OnInit, DoCheck {
  cmsPageContent: CmsPageContent;
  cmsNewsContent: CmsNewsContent;
  catalogueStats: CatalogueStats;
  isLoading: boolean;
  isBrowser = isPlatformBrowser(this.platformId);
  sectionParsed: { [section: string]: boolean } = {
    'page-content-section': false
  };
  year: number = new Date().getFullYear();
  month: string = intToMonth[new Date().getMonth()];

  /**
   * Class list in content that comes from CMS that will be treated as placeholder and replaced with dynamic content
   */
  classesToParse: Array<string> = [
    'elg-link',
    'photo-modal',
    'elg-catalogue-link',
    'elg-link-outlined',
    'elg-button-primary',
    'elg-button-accent',
    'elg-button-alternate',
    'topic-box',
    'become-provider-section',
    'become-provider-section-unregistered',
    'become-provider-section-applied',
    'widget-services',
    'widget-lr',
    'widget-organizations',
    'widget-paging-ts-back',
    'widget-paging-ts-next',
    'widget-paging-lr-back',
    'widget-paging-lr-next',
    'widget-paging-o-back',
    'widget-paging-o-next'
  ];

  private pageLoading = true;
  private readonly sub: any;

  constructor(
    private readonly config: ConfigService,
    private readonly cmsService: CmsService,
    private readonly cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    private readonly dynamicService: DynamicService,
    private readonly dynamicServerService: DynamicServerService,
    private readonly elRef: ElementRef,
    private readonly menuStore: Store<MenuState>,
    private readonly cmsStore: Store<State>,
    private readonly router: Router,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly renderer: Renderer2
  ) {
    super();

    this.cmsPageContent = new CmsPageContent();
    this.cmsNewsContent = new CmsNewsContent();
  }

  ngOnInit(): void {
    // const cmssettings = this.config.getSettings('system.cms_base_url');
    this.pageLoading = true;
    this.cmsStore.dispatch(new CmsAction.GetCmsPage('landing-new'));
    if (this.isBrowser) {
      this.cmsStore.dispatch(new CmsAction.GetCatalogueStatistics(this.config.getSettings('system.catalogue_url')));
    }

    this.menuStore.dispatch(new MenuAction.SetCurrentRoute(''));
    // tslint:disable-next-line:forin
    for (const i in this.sectionParsed) {
      this.sectionParsed[i] = false;
    }

    this.cmsStore
      .select((st: State) => st.cms)
      .pipe(
        map(cmsState => ((cmsState as unknown) as CmsState).serverData),
        filter(cmsState => cmsState !== null),
        distinctUntilChanged()
      )
      .subscribe(cont => {
        this.cmsPageContent.loadString(
          cont,
          this.config.getSettings('system.cms_base_url'),
          this.config.getSettings('system.cms_root_url'),
          this.config.getSettings('system.applicationUrl')
        );
        this.cmsPageContent.loading = false;
        this.isLoading = false;
        this.pageLoading = true;
        // tslint:disable-next-line:forin
        for (const i in this.sectionParsed) {
          this.sectionParsed[i] = false;
        }

        this.titleService.setTitle(this.cmsPageContent.title.replace('[no-display]', ''));
        // facebook
        this.metaService.addTag({ property: 'og:title', content: this.cmsPageContent.title.replace('[no-display]', '') });
        this.metaService.addTag({ property: 'og:description', content: this.cmsPageContent.subtitle });
        this.metaService.addTag({ property: 'og:type', content: 'website' });
        this.metaService.addTag({ property: 'og:url', content: `${this.config.getSettings('system.cms_base_url')}${this.router.url}` });
        this.metaService.addTag({
          property: 'og:image',
          content: `${this.config.getSettings('system.cms_base_url')}/assets/img/elg-logo.jpg`
        });
        this.metaService.addTag({ property: 'og:image:width', content: '1200' });
        this.metaService.addTag({ property: 'og:image:height', content: '628' });

        // twitter
        this.metaService.addTag({ property: 'twitter:title', content: this.cmsPageContent.title.replace('[no-display]', '') });
        this.metaService.addTag({
          property: 'twitter:url',
          content: `${this.config.getSettings('system.cms_base_url')}${this.router.url}`
        });
        this.metaService.addTag({ property: 'twitter:card', content: 'summary_large_image' });
        this.metaService.addTag({
          property: 'twitter:image',
          content: `${this.config.getSettings('system.cms_base_url')}/assets/img/elg-logo.jpg`
        });

        this.metaService.addTag({ name: 'title', content: this.cmsPageContent.title.replace('[no-display]', '') });
        this.metaService.addTag({ name: 'description', content: this.cmsPageContent.subtitle });

        this.cd.markForCheck();
      });

    this.isBrowser = isPlatformBrowser(this.platformId);

    this.cmsStore
      .select((st: State) => st.cms)
      .pipe(
        map(cmsState => ((cmsState as unknown) as CmsState).catalogueStats),
        filter(cmsState => cmsState !== null),
        distinctUntilChanged()
      )
      .subscribe(stats => {
        this.catalogueStats = stats;
        this.cd.markForCheck();
      });
  }

  ngDoCheck(): void {
    if (this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      const pageContentParsed = this.dynamicService.parseSection(
        'page-content-section',
        this.sectionParsed,
        this.cmsPageContent.body,
        this.elRef,
        '.page-content-section',
        this.classesToParse
      );
      if (pageContentParsed) {
        this.sectionParsed['page-content-section'] = true;
      }
      this.pageLoading = false;
      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
    } else if (!this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      const pageContentParsed = this.dynamicServerService.parseSection(
        'page-content-section',
        this.sectionParsed,
        this.cmsPageContent.body,
        this.elRef,
        '.page-content-section',
        this.classesToParse
      );
      const targetSection = this.renderer.selectRootElement('#page-content-section', true);
      const d2 = this.renderer.createElement('div');
      this.renderer.setProperty(d2, 'innerHTML', pageContentParsed.html());
      this.renderer.appendChild(targetSection, d2);

      if (pageContentParsed !== false) {
        this.sectionParsed['page-content-section'] = true;
      }
      this.pageLoading = false;
      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
    }
  }
}

