import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../framework/core';
import { MaterialModule } from '../framework/material';

import { ProfileComponent } from './profile.component';
import { routes } from './profile.routes';
import { ProviderApplicantService } from '../request-provider/provider-applicant.service';





@NgModule({
  imports: [
    CommonModule, 
    FormsModule, 
    ReactiveFormsModule, 
    RouterModule.forChild(routes), 
    SharedModule, 
    MaterialModule
  ],
  declarations: [
    ProfileComponent
  ],
  providers: [
    ProviderApplicantService
  ]

})
export class ProfileModule {}
