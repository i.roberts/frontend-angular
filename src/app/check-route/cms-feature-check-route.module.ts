import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckRouteComponent } from './check-route.component';
import { CheckRouteService } from './check-route.service';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [CheckRouteComponent],
  providers: [CheckRouteService],
  exports: [CheckRouteComponent],
})
export class CmsFeatureCheckRouteModule {}
