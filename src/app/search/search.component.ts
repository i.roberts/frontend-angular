import { isPlatformBrowser } from "@angular/common";
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

@Component({
  selector: 'app-search-component',
  templateUrl: './search.component.html',
  styleUrls: ['search.component.scss']
})
export class SearchComponent implements OnInit {
  myForm: FormGroup;
  searchInputDirty: boolean;
  searchPlaceHolder ='Search the catalogue';
  searchPosfix = "";
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    @Inject(PLATFORM_ID) private readonly platformId: any,
    ) { }

  ngOnInit(): void {
    this.isBrowser = isPlatformBrowser(this.platformId);
    this.searchInputDirty = false;
    this.createForm();

    this.router.events.subscribe((event: NavigationEnd | null) => {
      if (event instanceof NavigationEnd) {
        if (event.url === "/resources") {
          this.searchPosfix = '?resource_type__in=Lexical/Conceptual%20resource__Corpus__Language%20description'
        } else if (event.url === "/technologies") {
          this.searchPosfix = '?resource_type__term=Tool/Service'
        } else if (event.url === "/community") {
          this.searchPosfix = '?entity_type__in=Project__Organization'
        }
       }

     });

     if (this.route.snapshot.paramMap.get("alias") === "resources") {
      this.searchPosfix = '?resource_type__in=Lexical/Conceptual%20resource__Corpus__Language%20description'
    } else if (this.route.snapshot.paramMap.get("alias") === "technologies") {
      this.searchPosfix = '?resource_type__term=Tool/Service'
    } else if (this.route.snapshot.paramMap.get("alias") === "community") {
      this.searchPosfix = '?entity_type__in=Project__Organization'
    }

  }


  checkPlaceHolder(): void {
    if (this.searchPlaceHolder !== undefined) {
      this.searchPlaceHolder = undefined;

      return;
    } else {
      this.searchPlaceHolder = 'Search the catalogue';

      return;
    }
  }

  onSearchChange(value:string):void{
    if(value !== ''){
      this.searchInputDirty = true;
    }
  }

  cleanSearch(imp:any):void{
    if(imp.pageX !== 0) {
      this.searchInputDirty=false;
      this.myForm.get('searchTerm').setValue('');
    }
  }


  startSearch(searchTerm:string):void {

      if(this.isBrowser){
        window.location.href = `/catalogue/search/${encodeURIComponent(searchTerm)}${this.searchPosfix}`;
      }
  }


  private createForm():void {
    this.myForm = this.fb.group({
      searchTerm: ''
  });
  }

}
