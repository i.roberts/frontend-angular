import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Store } from '@ngrx/store';
import * as WebformAction from '../store/webforms/webform.actions';
import { State } from '../store/state';
import { routeAnimation } from '../shared';
import { KeycloakService } from 'keycloak-angular';
import { CookieService } from '../layout/foot/cookie.service';
import { ProviderApplicantService } from './provider-applicant.service';


@Component({
  selector: 'app-request-provider',
  templateUrl: './request-provider.component.html',
  styleUrls: ['./request-provider.component.css'],
  animations: [routeAnimation]
})
export class RequestProviderComponent implements OnInit {
  idToken: any;
  isBrowser = isPlatformBrowser(this.platformId);
  userIsProvider = false;
  userRequestedProviderRole = false;


  constructor(
    private readonly wfStore: Store<State>,
    private readonly keycloakService: KeycloakService,
    private readonly providerApplicant: ProviderApplicantService,
    @Inject(PLATFORM_ID) private readonly platformId: Object
  ) { }

  static fromEntries<T>(entries: Array<[keyof T, T[keyof T]]>): T {
    return entries.reduce(
      (acc, [key, value]) => ({ ...acc, [key]: value }),
      {} as T
    );
  }

  async ngOnInit(): Promise<void> {
    if (await this.keycloakService.isLoggedIn()) {
      this.idToken = this.keycloakService.getKeycloakInstance().idTokenParsed;

      if(this.providerApplicant.userIsProvider(this.idToken.roles)){
        this.userIsProvider = true;
      } else if (this.providerApplicant.userHasAppliedForProvider(this.idToken.sub)) {
        this.userRequestedProviderRole = true;
      }

      if (!this.providerApplicant.userIsProvider(this.idToken.roles) && !this.providerApplicant.userHasAppliedForProvider(this.idToken.sub) && this.isBrowser ) {
        this.providerApplicant.addProviderApplicant(this.idToken.sub);
        this.submitForm(this.idToken.given_name, this.idToken.family_name, this.idToken.email, this.idToken.affiliation, this.idToken.sub);

      } else if (this.providerApplicant.userIsProvider(this.idToken.roles) && this.providerApplicant.userHasAppliedForProvider(this.idToken.sub) && this.isBrowser) {
        this.providerApplicant.removeProviderApplicant(this.idToken.sub);
      }

    }
  }


  submitForm(name: string, last_name: string, email: string, affiliation: string, uid: string): void {
    const formObj = [];
    formObj.push( ["webform_id", "providers_request" ] );
    formObj.push( ["your_name", name ] );
    formObj.push( ["your_email", email ] );
    formObj.push( ["your_last_name", last_name ] );
    formObj.push( ["affiliation", affiliation ] );
    formObj.push( ["uid", uid ] );

    this.wfStore.dispatch(new WebformAction.PostWebform(RequestProviderComponent.fromEntries(formObj)));

  }



}
