### STAGE 1: Build ###

# We label our stage as ‘builder’
FROM node:10-alpine as builder

COPY package.json package-lock.json ./


## Storing node modules on a separate layer will prevent unnecessary npm installs at each build

RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run ng build -- --prod --output-path=dist


### STAGE 2: Setup ###
FROM nginx:1.14.1-alpine

## Copy our default nginx config && copy init script on container
COPY nginx/default.conf /etc/nginx/conf.d/
COPY entrypoint.sh /

## Remove default nginx website && and give ini script execute permission
RUN rm -rf /usr/share/nginx/html/* && \
apk add vim && \
chmod +x /entrypoint.sh

# Add initialization script
ENTRYPOINT ["/entrypoint.sh"]

COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
